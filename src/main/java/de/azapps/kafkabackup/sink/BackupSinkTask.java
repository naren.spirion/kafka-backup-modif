package de.azapps.kafkabackup.sink;

import de.azapps.kafkabackup.common.offset.EndOffsetReader;
import de.azapps.kafkabackup.common.offset.OffsetSink;
import de.azapps.kafkabackup.common.partition.PartitionIndex;
import de.azapps.kafkabackup.common.partition.PartitionWriter;
import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.segment.SegmentIndex;
import de.azapps.kafkabackup.common.segment.SegmentWriter;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.sink.SinkTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

// Importing Amazon SDK Libraries...
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetBucketAclRequest;
import software.amazon.awssdk.services.s3.model.GetBucketAclResponse;
import software.amazon.awssdk.services.s3.model.Grant;

public class BackupSinkTask extends SinkTask {
    private static final Logger log = LoggerFactory.getLogger(BackupSinkTask.class);
    private Path targetDir;
    private Map<TopicPartition, PartitionWriter> partitionWriters = new HashMap<>();
    private long maxSegmentSizeBytes;
    private OffsetSink offsetSink;
    private BackupSinkConfig config;
    private Map<TopicPartition, Long> endOffsets;
    private Map<TopicPartition, Long> currentOffsets = new HashMap<>();
    private EndOffsetReader endOffsetReader;
    private java.util.function.Consumer<Integer> exitFunction;

    // Trying to keep one S3Client per task... Let's see how it goes.
    private S3Client s3Client;

    // Bucket name.
    private String bucketName;

    // Boolean for task 0 - as per the current implementation, only this task is responsible for updating offset files.
    private boolean isTaskZero;

    @Override
    public String version() {
        return "0.1";
    }

    // Function that builds the S3Client. It gets called in start() during the setup of the sink task.
    private S3Client buildS3Client() {
        String accessKey = System.getenv("AWS_ACCESS_KEY_ID");
        String secretKey = System.getenv("AWS_SECRET_ACCESS_KEY");
        String regionName = System.getenv("AWS_DEFAULT_REGION");
        log.debug("Access key: " + accessKey);
        log.debug("Secret key: " + secretKey);
        AwsSessionCredentials sessionCredentials = AwsSessionCredentials
                .create(accessKey, secretKey, "");
        S3Client newS3Client = S3Client
                .builder()
                .credentialsProvider(StaticCredentialsProvider.create(sessionCredentials))
                .region(Region.of(regionName))
                .build();
        log.info("Created a new S3 Client for BackupSinkTask.");
        return newS3Client;
    }

    @Override
    public void start(Map<String, String> props) {
        start(props, null, null, null);
    }

    public void start(
            Map<String, String> props,
            OffsetSink overrideOffsetSink,
            EndOffsetReader overrideEndOffsetReader,
            java.util.function.Consumer<Integer> overrideExitFunction
    ) {
        this.config = new BackupSinkConfig(props);

        // Not sure if this is really required? Need to check.
        Integer taskId = Integer.valueOf(props.get("task.id"));
        this.isTaskZero = taskId.equals(0);

        try {
            maxSegmentSizeBytes = config.maxSegmentSizeBytes();
            targetDir = Paths.get(config.targetDir());
            bucketName = config.bucketName();
            Files.createDirectories(targetDir);

            if (!bucketName.equals("null")) {
                this.s3Client = buildS3Client();
                GetBucketAclResponse response = s3Client.getBucketAcl(GetBucketAclRequest.builder().bucket(bucketName).build());
                for (Grant grant : response.grants()) {
                    log.info(grant.permissionAsString());
                }
            }

            // TODO: Maybe check if the bucket doesn't exist, and create it if possible? Not sure how this will affect AWS credential requirements.

            // Allow tests to use mock offset sync
            if(overrideOffsetSink != null) {
                offsetSink = overrideOffsetSink;
            } else {
                AdminClient adminClient = AdminClient.create(config.adminConfig());
                if (bucketName.equals("null")) {
                    offsetSink = new OffsetSink(adminClient, targetDir);
                } else {
                    offsetSink = new OffsetSink(adminClient, targetDir, log, bucketName, s3Client);
                }
            }

            if (overrideEndOffsetReader != null) {
                this.endOffsetReader = overrideEndOffsetReader;
            } else {
                endOffsetReader = new EndOffsetReader(config.consumerConfig());
            }

            if (overrideExitFunction != null) {
                this.exitFunction = overrideExitFunction;
            } else {
                this.exitFunction = System::exit;
            }

            log.debug("Initialized BackupSinkTask with target dir {}", targetDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Check for end-offsets. Terminate if all offsets >= end-offsets
     */
    private void terminateIfCompleted() {
        boolean terminate = true;
        for (Map.Entry<TopicPartition, Long> partitionOffset : endOffsets.entrySet()) {
            Long endOffset = partitionOffset.getValue();
            Long currentOffset = currentOffsets.getOrDefault(partitionOffset.getKey(), -1L);

            if (currentOffset < endOffset - 1) {
                return;
            }
        }
        if (terminate) {
            log.debug("Snapshot complete. Terminating kafka connect.");
            stop(); // seems that this is not called when using System.exit()
            exitFunction.accept(0);
        }
    }

    @Override
    public void put(Collection<SinkRecord> records) {
        try {
            for (SinkRecord sinkRecord : records) {
                TopicPartition topicPartition = new TopicPartition(sinkRecord.topic(), sinkRecord.kafkaPartition());
                PartitionWriter partition = partitionWriters.get(topicPartition);
                partition.append(Record.fromSinkRecord(sinkRecord));
                if (sinkRecord.kafkaOffset() % 5 == 0) { // TODO: Remember to increase the modulo value later.
                    log.debug("Backed up Topic {}, Partition {}, up to offset {}", sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
                }
                // We'd want to occasionally update the partition index to account for segments deleted in S3.
                if (!bucketName.equals("null") && partition.getNewSegments() == 200) {
                    partition.setNewSegments(0);
                    partition.updatePartitionIndex();
                }
                if (config.snapShotMode()) {
                    currentOffsets.put(topicPartition, sinkRecord.kafkaOffset());
                }
            }

            // Todo: refactor to own worker. E.g. using the scheduler of MM2
            if (isTaskZero) {
                offsetSink.syncConsumerGroups();
                offsetSink.syncOffsets();
            }

            if (config.snapShotMode()) {
                terminateIfCompleted();
            }
        } catch (IOException | SegmentIndex.IndexException | PartitionIndex.IndexException | SegmentWriter.SegmentException e) {
            throw new RuntimeException(e);
        }
    }

    public void open(Collection<TopicPartition> partitions) {
        super.open(partitions);

        try {
            for (TopicPartition topicPartition : partitions) {
                Path topicDir = Paths.get(targetDir.toString(), topicPartition.topic());
                Files.createDirectories(topicDir);

                PartitionWriter partitionWriter;

                // Modified constructor to pass the bucket name.
                if (bucketName.equals("null")) {
                    partitionWriter = new PartitionWriter(
                            topicPartition.topic(),
                            topicPartition.partition(),
                            topicDir,
                            maxSegmentSizeBytes
                    );
                } else {
                    partitionWriter = new PartitionWriter(
                            topicPartition.topic(),
                            topicPartition.partition(),
                            topicDir,
                            maxSegmentSizeBytes,
                            bucketName,
                            s3Client,
                            log
                    );
                }

                log.debug("Started a new Partition Writer Object.");
                log.debug(bucketName + " <- This is the bucket we are backing up to.");

                long lastWrittenOffset = partitionWriter.lastWrittenOffset();

                // Note that we must *always* request that we seek to an offset here. Currently, the
                // framework will still commit Kafka offsets even though we track our own (see KAFKA-3462),
                // which can result in accidentally using that offset if one was committed but no files
                // were written to disk. To protect against this, even if we
                // just want to start at offset 0 or reset to the earliest offset, we specify that
                // explicitly to forcibly override any committed offsets.

                if (lastWrittenOffset > 0) {
                    context.offset(topicPartition, lastWrittenOffset + 1);
                    log.debug("Initialized Topic {}, Partition {}. Last written offset: {}"
                            , topicPartition.topic(), topicPartition.partition(), lastWrittenOffset);
                } else {
                    // The offset was not found, so rather than forcibly set the offset to 0 we let the
                    // consumer decide where to start based upon standard consumer offsets (if available)
                    // or the consumer's `auto.offset.reset` configuration

                    // if we are in snapshot mode, then just start at zero.
                    if (config.snapShotMode()) {
                        context.offset(topicPartition, 0);
                    }

                    log.info("Resetting offset for {} based upon existing consumer group offsets or, if "
                            + "there are none, the consumer's 'auto.offset.reset' value.", topicPartition);
                }

                this.partitionWriters.put(topicPartition, partitionWriter);
                this.currentOffsets.put(topicPartition, lastWrittenOffset);

            }
            if ( config.snapShotMode() ) {
                this.endOffsets = endOffsetReader.getEndOffsets(partitions);
                this.terminateIfCompleted();
            }
            if (partitions.isEmpty()) {
                log.info("No partitions assigned to BackupSinkTask");
            }
        } catch (IOException | SegmentIndex.IndexException | PartitionIndex.IndexException | SegmentWriter.SegmentException e) {
            throw new RuntimeException(e);
        }

    }

    public void close(Collection<TopicPartition> partitions) {
        super.close(partitions);
        try {
            for (TopicPartition topicPartition : partitions) {
                PartitionWriter partitionWriter = partitionWriters.get(topicPartition);
                if (partitionWriter != null) {
                    partitionWriter.close();
                }
                partitionWriters.remove(topicPartition);
                log.debug("Closed BackupSinkTask for Topic {}, Partition {}"
                        , topicPartition.topic(), topicPartition.partition());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() {
        try {
            for (PartitionWriter partition : partitionWriters.values()) {
                partition.close();
            }
            offsetSink.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (s3Client != null) {
            s3Client.close(); // Need to close the client associated with the task.
        }
        log.info("Stopped BackupSinkTask");
    }

    @Override
    public void flush(Map<TopicPartition, OffsetAndMetadata> currentOffsets) {
        try {
            for (PartitionWriter partitionWriter : partitionWriters.values()) {
                partitionWriter.flush();
                log.debug("Flushed Topic {}, Partition {}"
                        , partitionWriter.topic(), partitionWriter.partition());
            }
            offsetSink.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
