package de.azapps.kafkabackup.source;

import de.azapps.kafkabackup.common.offset.OffsetSource;
import de.azapps.kafkabackup.common.partition.PartitionIndex;
import de.azapps.kafkabackup.common.partition.PartitionReader;
import de.azapps.kafkabackup.common.partition.PartitionUtils;
import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.segment.SegmentIndex;
import de.azapps.kafkabackup.common.segment.SegmentReader;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;

public class BackupSourceTask extends SourceTask {
    private static final Logger log = LoggerFactory.getLogger(BackupSourceTask.class);
    private static final String SOURCE_PARTITION_PARTITION = "Partition";
    private static final String SOURCE_PARTITION_TOPIC = "Topic";
    private static final String SOURCE_OFFSET_OFFSET = "Offset";
    private Path sourceDir;
    private final Map<TopicPartition, PartitionReader> partitionReaders = new HashMap<>();
    private final Set<TopicPartition> finishedPartitions = new HashSet<>();
    private int batchSize = 100;
    private OffsetSource offsetSource;
    private List<String> topics;

    // S3 Client.
    private S3Client s3Client;

    // Bucket name.
    private String bucketName;

    @Override
    public String version() {
        return "0.1";
    }

    // Function that builds the S3Client. It gets called in start() during the setup of the sink task.
    private S3Client buildS3Client() {
        String accessKey = System.getenv("AWS_ACCESS_KEY_ID");
        String secretKey = System.getenv("AWS_SECRET_ACCESS_KEY");
        String regionName = System.getenv("AWS_DEFAULT_REGION");
        log.debug("Access key: " + accessKey);
        log.debug("Secret key: " + secretKey);
        AwsSessionCredentials sessionCredentials = AwsSessionCredentials
                .create(accessKey, secretKey, "");
        S3Client newS3Client = S3Client
                .builder()
                .credentialsProvider(StaticCredentialsProvider.create(sessionCredentials))
                .region(Region.of(regionName))
                .build();
        log.info("Created a new S3 Client for BackupSourceTask.");
        return newS3Client;
    }

    @Override
    public void start(Map<String, String> props) {
        BackupSourceConfig config = new BackupSourceConfig(props);
        this.sourceDir = Paths.get(config.sourceDir());
        this.batchSize = config.batchSize();
        this.topics = config.topics();

        bucketName = config.bucketName();

        if (!bucketName.equals("null")) {
            this.s3Client = buildS3Client();
            GetBucketAclResponse response = s3Client.getBucketAcl(GetBucketAclRequest.builder().bucket(bucketName).build());
            for (Grant grant : response.grants()) {
                log.info(grant.permissionAsString());
            }
        }

        try {
            findPartitions();
            if (bucketName.equals("null")) {
                offsetSource = new OffsetSource(sourceDir, topics, config.consumerConfig());
            } else {
                offsetSource = new OffsetSource(sourceDir, topics, config.consumerConfig(), bucketName, s3Client, log);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Some partitions may already be complete.
        Set<TopicPartition> toNotRestore = new HashSet<>();

        for (Map.Entry<TopicPartition, PartitionReader> entry : partitionReaders.entrySet()) {
            TopicPartition topicPartition = entry.getKey();
            PartitionReader partitionReader = entry.getValue();

            long finalOffset = partitionReader.getLastBackedUpOffset();
            log.info("Last backed up offset for topic {}, partition {} is considered to be {}"
                    , topicPartition.topic(), topicPartition.partition(), finalOffset);

            String timeStamp = config.getToTimestampConfig();
            // If the ending timestamp is given, we check which offset that corresponds to.
            // If not, we stop at the last currently backed up offset (whichever is earlier).
            if (!timeStamp.equals("")) {
                long toTimestamp = Instant.parse(timeStamp).toEpochMilli();
                try {
                    long timestampOffset = partitionReader.findOffsetFromTimestamp(toTimestamp + 1) - 1;
                    log.info("Timestamp offset: {}", timestampOffset);
                    finalOffset = Math.min(finalOffset, timestampOffset);
                    log.info("Final offset for topic partition {}: {}",
                            topicPartition, finalOffset);
                } catch (Exception e) {
                    log.info("Error while finding final offset to restore until: {}", e.getMessage());
                }
            }
            partitionReader.setFinalOffset(finalOffset);

            timeStamp = config.getFromTimestampConfig();
            log.info("Starting timestamp = [{}]", timeStamp);
            // In case the starting timestamp is given, we need to seek to the relevant starting offset.
            // Otherwise, we use whatever source offsets are available (or seek to the beginning if they don't exist).
            if (!timeStamp.equals("")) {
                long startOffset = (long) 9e18;
                long fromTimestamp = Instant.parse(timeStamp).toEpochMilli();
                try {
                    startOffset = partitionReader.findOffsetFromTimestamp(fromTimestamp);
                    log.info("Start offset for topic partition {}: {}",
                            topicPartition, startOffset);
                } catch (Exception e) {
                    log.error("Error while finding starting offset to restore from: {}", e.getMessage());
                }
                if (startOffset > finalOffset) { // No point in restoring here.
                    log.info("Not restoring topic partition {}, required starting offset {} is higher than the ending offset {}",
                            topicPartition, startOffset, finalOffset);
                    toNotRestore.add(topicPartition);
                } else {
                    try {
                        partitionReader.seek(startOffset);
                    } catch (Exception e) {
                        log.error("Unable to seek to starting offset {} for topic {} partition {}, not restoring.",
                                startOffset, topicPartition.topic(), topicPartition.partition());
                        toNotRestore.add(topicPartition);
                    }
                }
                continue;
            }

            Map<String, String> sourcePartition = new HashMap<>();
            sourcePartition.put(SOURCE_PARTITION_TOPIC, topicPartition.topic());
            sourcePartition.put(SOURCE_PARTITION_PARTITION, String.valueOf(topicPartition.partition()));
            Map<String, Object> sourceOffset = context.offsetStorageReader().offset(sourcePartition);
            if (sourceOffset != null) {
                try {
                    // seek() seeks to the position of the OFFSET. We need to move to the position after the current OFFSET.
                    // Otherwise, we would write the OFFSET multiple times in case of a restart

                    partitionReader.seek((Long) sourceOffset.get(SOURCE_OFFSET_OFFSET));
                    if (partitionReader.hasMoreData()) {
                        partitionReader.read();
                    }
                } catch (
                        IOException |
                        SegmentIndex.IndexException |
                        PartitionIndex.IndexException |
                        SegmentReader.SegmentException e
                ) {
                    throw new RuntimeException(e);
                }
            }
        }

        for (TopicPartition topicPartition : toNotRestore) {
            partitionReaders.remove(topicPartition);
        }
    }

    private void findPartitions() throws IOException {
        if (!bucketName.equals("null")) {
            for (String topic : topics) {
                // Check if the topic prefix is present in the bucket.
                // Check all files with that topic prefix.
                // Try to create a new PartitionReader object on the partition index files.

                Path tempDir = Paths.get(sourceDir.toString(), topic);

                // Making a folder with the topic name if it doesn't exist already.
                if (!Files.isDirectory(tempDir)) {
                    Files.createDirectory(tempDir);
                    log.debug("Directory for topic " + tempDir + " created.");
                }

                ListObjectsRequest request = ListObjectsRequest
                        .builder()
                        .bucket(bucketName)
                        .prefix(topic)
                        .build();
                ListObjectsResponse response = s3Client.listObjects(request);

                // Handles the case where the request is truncated.
                while (true) {
                    response.contents().forEach(content -> {
                        String fullPath = content.key();
                        assert (fullPath.startsWith(topic));
                        String fileName = fullPath.substring(topic.length() + 1);

                        Optional<Integer> partition = PartitionUtils.isPartitionIndex(Paths.get(fileName));
                        if (partition.isPresent()) {
                            TopicPartition topicPartition = new TopicPartition(topic, partition.get());
                            PartitionReader partitionReader;
                            try {
                                partitionReader = new PartitionReader(topic, partition.get(), tempDir, bucketName, s3Client, log);
                            } catch (
                                    IOException |
                                    PartitionIndex.IndexException |
                                    PartitionReader.PartitionException |
                                    SegmentIndex.IndexException |
                                    SegmentReader.SegmentException e
                            ) {
                                throw new RuntimeException(e);
                            }
                            partitionReaders.put(topicPartition, partitionReader);
                            log.info("Registered topic {} partition {}", topic, partition.get());
                        }
                    });
                    if (response.isTruncated()) {
                        response = s3Client.listObjects(
                                ListObjectsRequest
                                        .builder()
                                        .bucket(bucketName)
                                        .prefix(topic)
                                        .delimiter(response.nextMarker())
                                        .build()
                        );
                    } else {
                        break;
                    }
                }
            }
        } else {
            for (String topic : topics) {
                Path topicDir = Paths.get(sourceDir.toString(), topic);
                if (!Files.isDirectory(topicDir)) {
                    throw new RuntimeException("Missing directory for topic " + topic);
                }
                Files.list(topicDir).forEach((Path f) -> {
                    Optional<Integer> partition = PartitionUtils.isPartitionIndex(f);
                    if (partition.isPresent()) {
                        TopicPartition topicPartition = new TopicPartition(topic, partition.get());
                        PartitionReader partitionReader;
                        try {
                            partitionReader = new PartitionReader(topic, partition.get(), topicDir);
                        } catch (
                                IOException |
                                PartitionIndex.IndexException |
                                PartitionReader.PartitionException |
                                SegmentIndex.IndexException |
                                SegmentReader.SegmentException e
                        ) {
                            throw new RuntimeException(e);
                        }
                        partitionReaders.put(topicPartition, partitionReader);
                        log.info("Registered topic {} partition {}", topic, partition);
                    }
                });
            }
        }
    }

    long lastPrint = 0;

    @Override
    public List<SourceRecord> poll() {
        List<SourceRecord> sourceRecords = new ArrayList<>();
        if (finishedPartitions.equals(partitionReaders.keySet())) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastPrint > 5000) {
                log.info("All records read. Restore was successful");
                lastPrint = currentTime;
            }
            return new ArrayList<>();
        }
        try {
            for (Map.Entry<TopicPartition, PartitionReader> entry : partitionReaders.entrySet()) {
                TopicPartition topicPartition = entry.getKey();
                PartitionReader partitionReader = entry.getValue();

                List<Record> records = partitionReader.readBytesBatch(batchSize);
                if (records.size() > 0) {
                    log.info("Read {} record(s) from topic {} partition {}. Current offset: {}",
                            records.size(), records.get(0).topic(), records.get(0).kafkaPartition(), records.get(records.size() - 1).kafkaOffset());
                }
                for (Record record : records) {
                    sourceRecords.add(toSourceRecord(record));
                }
                if (!partitionReader.hasMoreData()) {
                    finishedPartitions.add(topicPartition);
                }
            }
        } catch (IOException | SegmentIndex.IndexException e) {
            e.printStackTrace();
        }
        return sourceRecords;
    }

    private SourceRecord toSourceRecord(Record record) {
        Map<String, String> sourcePartition = new HashMap<>();
        sourcePartition.put(SOURCE_PARTITION_PARTITION, record.kafkaPartition().toString());
        sourcePartition.put(SOURCE_PARTITION_TOPIC, record.topic());
        Map<String, Long> sourceOffset = Collections.singletonMap(SOURCE_OFFSET_OFFSET, record.kafkaOffset());
        ConnectHeaders connectHeaders = new ConnectHeaders();
        for (Header header : record.headers()) {
            connectHeaders.addBytes(header.key(), header.value());
        }
        return new SourceRecord(sourcePartition, sourceOffset,
                record.topic(), record.kafkaPartition(),
                Schema.OPTIONAL_BYTES_SCHEMA, record.key(),
                Schema.OPTIONAL_BYTES_SCHEMA, record.value(),
                record.timestamp(), connectHeaders);
    }

    @Override
    public void commitRecord(SourceRecord record, RecordMetadata metadata) {
        TopicPartition topicPartition = new TopicPartition(metadata.topic(), metadata.partition());
        long sourceOffset = (Long) record.sourceOffset().get(SOURCE_OFFSET_OFFSET);
        long targetOffset = metadata.offset();
        offsetSource.syncGroupForOffset(topicPartition, sourceOffset, targetOffset);
    }

    @Override
    public void stop() {
        for (PartitionReader partitionReader : partitionReaders.values()) {
            try {
                partitionReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (s3Client != null) {
            s3Client.close(); // Need to close the client associated with the task.
        }
        log.info("Stopped BackupSourceTask");
    }
}
