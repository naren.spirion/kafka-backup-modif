package de.azapps.kafkabackup.common.partition;

import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.segment.SegmentIndex;
import de.azapps.kafkabackup.common.segment.SegmentWriter;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class PartitionWriter {
    private String topic;
    private int partition;
    private Path topicDir;
    private SegmentWriter currentSegment;
    private PartitionIndex partitionIndex;
    private long maxSegmentSizeBytes;

    // Bucket name & Path to the partition index in the bucket & S3 Client.
    private String bucketName;
    private Path partitionIndexKeyFile;
    private S3Client s3Client;

    // Logging in this class too.
    private Logger log;

    // Number of new segment details added to the existing partition index since last truncation.
    // This is used as a metric to determine when to update (truncate) the partition index
    // (to account for the eventual deletion of objects in the case of backups to S3).
    private int newSegments = 0;

    public int getNewSegments() {
        return newSegments;
    }
    public void setNewSegments(int value) {
        newSegments = value;
    };


    // The regular constructor.
    public PartitionWriter(String topic, int partition, Path topicDir, long maxSegmentSizeBytes) throws IOException, PartitionIndex.IndexException, SegmentIndex.IndexException {
        this.topic = topic;
        this.partition = partition;
        this.topicDir = topicDir;
        this.maxSegmentSizeBytes = maxSegmentSizeBytes;
        Path indexFile = PartitionUtils.indexFile(topicDir, partition);
        if (!Files.isDirectory(this.topicDir)) {
            Files.createDirectories(this.topicDir);
        }
        partitionIndex = new PartitionIndex(indexFile);
        Optional<PartitionIndexEntry> optionalPartitionIndexEntry = partitionIndex.latestSegmentFile();
        if (optionalPartitionIndexEntry.isPresent()) {
            currentSegment = new SegmentWriter(topic, partition, optionalPartitionIndexEntry.get().startOffset(), topicDir);
        } else {
            currentSegment = new SegmentWriter(topic, partition, 0, topicDir);
            // do not forget to add the current segment to the partition index. Even if it is empty
            // Starting timestamp of the segment can be considered as -1 when rolling the first one.
            partitionIndex.appendSegment(currentSegment.filePrefix(), 0, -1);
        }
    }

    // Enhanced constructor that includes the bucket name & s3 client.
    public PartitionWriter(
            String topic,
            int partition,
            Path topicDir,
            long maxSegmentSizeBytes,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, PartitionIndex.IndexException, SegmentIndex.IndexException, SegmentWriter.SegmentException {
        this.topic = topic;
        this.partition = partition;
        this.topicDir = topicDir;
        this.maxSegmentSizeBytes = maxSegmentSizeBytes;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        Path indexFile = PartitionUtils.indexFile(topicDir, partition);

        // Path for the partition index in the bucket.
        this.partitionIndexKeyFile = PartitionUtils.indexFile(Paths.get(topic), partition);

        // This won't be needed for the S3 backup, just the topic is needed for the object path, this is for local organisation...
        if (!Files.isDirectory(this.topicDir)) {
            Files.createDirectories(this.topicDir);
        }
        // Loading the partition index will be crucial.
        partitionIndex = new PartitionIndex(
                indexFile,
                partitionIndexKeyFile,
                bucketName,
                s3Client,
                log
        );

        Optional<PartitionIndexEntry> optionalPartitionIndexEntry = partitionIndex.latestSegmentFile();
        if (optionalPartitionIndexEntry.isPresent()) {
            // Enhanced SegmentWriter constructors.
            currentSegment = new SegmentWriter(topic, partition, optionalPartitionIndexEntry.get().startOffset(), topicDir, bucketName, s3Client, log);
        } else {
            currentSegment = new SegmentWriter(topic, partition, 0, topicDir, bucketName, s3Client, log);
            // do not forget to add the current segment to the partition index. Even if it is empty
            // Starting timestamp of the segment can be considered as -1 when rolling the first one.
            partitionIndex.appendSegment(currentSegment.filePrefix(), 0, -1);
        }
    }

    public int partitionIndexSize() {
        return partitionIndex.index().size();
    }

    public void updatePartitionIndex() throws IOException {
        if (partitionIndexSize() <= 1) {
            return;
        }
        partitionIndex.truncatePartitionIndex();
        partitionIndex.rewriteFile();
        partitionIndex.uploadToS3();
    }

    private void nextSegment(Record record) throws IOException, SegmentIndex.IndexException, PartitionIndex.IndexException, SegmentWriter.SegmentException {
        long startOffset = record.kafkaOffset();
        long timestamp = record.timestamp();
        long latestTimestamp = currentSegment.latestWrittenTimestamp();
        if (timestamp < latestTimestamp) {
            timestamp = latestTimestamp;
        }

        currentSegment.close();
        // Make sure the SegmentWriter is specified correctly!
        SegmentWriter segment = null;
        if (bucketName == null) {
            segment = new SegmentWriter(topic, partition, startOffset, topicDir);
        } else {
            try {
                segment = new SegmentWriter(topic, partition, startOffset, topicDir, bucketName, s3Client, log);
            } catch (SegmentWriter.SegmentException e) {
                log.error("Error in creating new SegmentWriter object.");
            }
        }
        if (startOffset > partitionIndex.latestStartOffset()) {
            partitionIndex.appendSegment(segment.filePrefix(), startOffset, timestamp);
        }
        newSegments++;
        currentSegment = segment;
        currentSegment.setLastWrittenTimestamp(timestamp);
        currentSegment.append(record);
    }

    public long lastWrittenOffset() {
        return currentSegment.lastWrittenOffset();
    }

    public void append(Record record) throws IOException, SegmentIndex.IndexException, PartitionIndex.IndexException, SegmentWriter.SegmentException {
        if (currentSegment.size() > maxSegmentSizeBytes) {
            nextSegment(record);
        } else {
            currentSegment.append(record);
        }
    }

    public void close() throws IOException {
        partitionIndex.close();
        currentSegment.close();
    }

    public void flush() throws IOException {
        partitionIndex.flush();
        currentSegment.flush();
    }

    public String topic() {
        return topic;
    }

    public int partition() {
        return partition;
    }
}
