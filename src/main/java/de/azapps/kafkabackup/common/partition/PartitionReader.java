package de.azapps.kafkabackup.common.partition;

import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.segment.SegmentIndex;
import de.azapps.kafkabackup.common.segment.SegmentReader;
import de.azapps.kafkabackup.common.segment.SegmentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.s3.S3Client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PartitionReader {
    private final String topic;
    private final int partition;
    private Path topicDir;
    private SegmentReader currentSegment;
    private final PartitionIndex partitionIndex;

    // Bucket name & S3 Client.
    private String bucketName;
    private S3Client s3Client;

    // Logging in this class too.
    private Logger log;

    // This is where files will be downloaded into from S3 and read into Kafka.
    private Path tempDir;

    // Final offset (to stop backup at)
    private long finalOffset = -1;

    // Regular constructor.
    public PartitionReader(String topic, int partition, Path topicDir) throws IOException, PartitionIndex.IndexException, PartitionException, SegmentIndex.IndexException, SegmentReader.SegmentException {
        this.topic = topic;
        this.partition = partition;
        this.topicDir = topicDir;
        Path indexFile = PartitionUtils.indexFile(topicDir, partition);
        if (!Files.isDirectory(this.topicDir)) {
            throw new PartitionException("Cannot find topic directory for topic " + topic);
        }
        if (!Files.isRegularFile(indexFile)) {
            throw new PartitionException("Cannot find index file for partition " + partition);
        }
        partitionIndex = new PartitionIndex(indexFile);
        if (partitionIndex.hasMoreData()) {
            seek(partitionIndex.firstOffset());
        }
    }

    // Enhanced constructor.
    public PartitionReader(
            String topic,
            int partition,
            Path tempDir,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, PartitionIndex.IndexException, PartitionException, SegmentIndex.IndexException, SegmentReader.SegmentException {
        this.topic = topic;
        this.partition = partition;
        this.tempDir = tempDir;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        // Where we want to place the partition index file locally.
        Path indexFile = PartitionUtils.indexFile(tempDir, partition);

        // Where the partition index file is present in S3.
        Path partitionIndexKeyFile = PartitionUtils.indexFile(Paths.get(topic), partition);

        // Calling the PartitionIndex constructor enhanced in the sink task.
        partitionIndex = new PartitionIndex(
                indexFile,
                partitionIndexKeyFile,
                bucketName,
                s3Client,
                log
        );

        // Truncate the partition index (in case segment files have been deleted).
        partitionIndex.truncatePartitionIndex();

        if (partitionIndex.hasMoreData()) {
            seek(partitionIndex.firstOffset());
        }
    }

    public void setFinalOffset(long offset) {
        this.finalOffset = offset;
        partitionIndex.setFinalOffset(offset);
    }

    public long getLastBackedUpOffset() {
        return partitionIndex.getLastBackedUpOffset();
    }

    public void close() throws IOException {
        partitionIndex.closeRestore();
        if (currentSegment != null) {
            currentSegment.close();
        }
    }

    // Finds the first offset with the index timestamp >= the given timestamp.
    public long findOffsetFromTimestamp(long timestamp) throws IOException, SegmentIndex.IndexException, IndexOutOfBoundsException, SegmentReader.SegmentException {
        partitionIndex.seekTimestamp(timestamp);
        String segmentFilePrefix = partitionIndex.readFileName();
        if (bucketName == null) {
            currentSegment = new SegmentReader(topic, partition, topicDir, segmentFilePrefix);
        } else {
            // Use the enhanced SegmentReader, for restoring from S3.
            // If the segment file doesn't exist, object lifecycle management may have deleted it.
            // In that case, continue to the next segment.
            while (!partitionIndex.keyExists(bucketName, Paths.get(topic,segmentFilePrefix + "_index").toString())) {
                segmentFilePrefix = partitionIndex.readFileName();
            }
            currentSegment = new SegmentReader(topic, partition, tempDir, segmentFilePrefix, bucketName, s3Client, log);
        }
        return currentSegment.findOffsetFromTimestamp(timestamp);
    }

    public void seek(long offset) throws PartitionIndex.IndexException, IOException, SegmentIndex.IndexException, IndexOutOfBoundsException, SegmentReader.SegmentException {
        partitionIndex.seek(offset);
        String segmentFilePrefix = partitionIndex.readFileName();
        if (bucketName == null) {
            currentSegment = new SegmentReader(topic, partition, topicDir, segmentFilePrefix);
        } else {
            // Use the enhanced SegmentReader, for restoring from S3.
            // If the segment file doesn't exist, object lifecycle management may have deleted it.
            // In that case, continue to the next segment.
            while (!partitionIndex.keyExists(bucketName, Paths.get(topic,segmentFilePrefix + "_index").toString())) {
                segmentFilePrefix = partitionIndex.readFileName();
            }
            currentSegment = new SegmentReader(topic, partition, tempDir, segmentFilePrefix, bucketName, s3Client, log);
        }
        if (finalOffset != -1) {
            currentSegment.setFinalOffset(finalOffset);
        }
        currentSegment.seek(offset);
    }

    public boolean hasMoreData() throws IOException {
        if (currentSegment != null) {
            return currentSegment.hasMoreData() || partitionIndex.hasMoreData();
        } else {
            return false;
        }
    }

    public Record read() throws IOException, SegmentIndex.IndexException {
        if (currentSegment.hasMoreData()) {
            return currentSegment.read();
        } else if (partitionIndex.hasMoreData()) {
            currentSegment.close();
            String segmentFilePrefix = partitionIndex.readFileName();
            if (bucketName == null) {
                currentSegment = new SegmentReader(topic, partition, topicDir, segmentFilePrefix);
            } else {
                // If the segment file doesn't exist, object lifecycle management may have deleted it.
                // In that case, continue to the next segment.
                while (!partitionIndex.keyExists(bucketName, Paths.get(topic,segmentFilePrefix + "_index").toString())) {
                    segmentFilePrefix = partitionIndex.readFileName();
                }
                try {
                    currentSegment = new SegmentReader(topic, partition, tempDir, segmentFilePrefix, bucketName, s3Client, log);
                } catch (SegmentReader.SegmentException e) {
                    log.info("Error in opening a new segment reader.");
                }
            }
            if (finalOffset != -1) {
                currentSegment.setFinalOffset(finalOffset);
            }
            return currentSegment.read();
        } else {
            throw new IndexOutOfBoundsException("No more data available");
        }
    }

    public List<Record> readN(int n) throws IOException, SegmentIndex.IndexException {
        List<Record> records = new ArrayList<>();
        while (hasMoreData() && records.size() < n) {
            Record record = read();
            records.add(record);
        }
        return records;
    }

    public List<Record> readBytesBatch(long batchsize) throws IOException, SegmentIndex.IndexException {
        List<Record> records = new ArrayList<>();
        long currentSize = 0;
        while (hasMoreData() && currentSize < batchsize) {
            Record record = read();
            records.add(record);
            if (record.value() != null) {
                currentSize += record.value().length;
            }
            if (record.key() != null) {
                currentSize += record.key().length;
            }
        }
        return records;
    }


    public List<Record> readFully() throws IOException, SegmentIndex.IndexException {
        List<Record> records = new ArrayList<>();
        while (hasMoreData()) {
            Record record = read();
            records.add(record);
        }
        return records;
    }


    public static class PartitionException extends Exception {
        PartitionException(String message) {
            super(message);
        }
    }
}
