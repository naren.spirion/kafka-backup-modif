package de.azapps.kafkabackup.common.partition;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Format:
 * fileNameLength: int32
 * fileName: UTF8-String[fileNameLength]
 * startOffset: int64
 * [endOffset: int64]
 */
public class PartitionIndexEntry {
    private final String filename;
    private final long startOffset;
    private final long startTimestamp;

    PartitionIndexEntry(OutputStream byteStream, String filename, long startOffset, long startTimestamp) throws IOException {
        this.filename = filename;
        this.startOffset = startOffset;
        this.startTimestamp = startTimestamp;
        DataOutputStream stream = new DataOutputStream(byteStream);
        byte[] filenameBytes = filename.getBytes(StandardCharsets.UTF_8);
        stream.writeInt(filenameBytes.length);
        stream.write(filenameBytes);
        stream.writeLong(startOffset);
        stream.writeLong(startTimestamp);
    }

    PartitionIndexEntry(String filename, long startOffset, long startTimestamp) {
        this.filename = filename;
        this.startOffset = startOffset;
        this.startTimestamp = startTimestamp;
    }

    static PartitionIndexEntry fromStream(InputStream byteStream) throws IOException {
        DataInputStream stream = new DataInputStream(byteStream);
        int filenameLength = stream.readInt();
        byte[] filenameBytes = new byte[filenameLength];
        int readBytes = stream.read(filenameBytes);
        if (readBytes != filenameLength) {
            throw new IOException(String.format("Expected to read %d bytes, got %d", filenameLength, readBytes));
        }
        String filename = new String(filenameBytes, StandardCharsets.UTF_8);
        long startOffset = stream.readLong();
        long startTimestamp = stream.readLong();
        return new PartitionIndexEntry(filename, startOffset, startTimestamp);
    }

    public long startTimestamp() {
        return startTimestamp;
    }

    public long startOffset() {
        return startOffset;
    }

    public String filename() {
        return filename;
    }

    @Override
    public int hashCode() {
        return Objects.hash(filename, startOffset);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PartitionIndexEntry that = (PartitionIndexEntry) o;

        return Objects.equals(filename(), that.filename())
                && Objects.equals(startOffset(), that.startOffset());
    }

    @Override
    public String toString() {
        return String.format("PartitionIndexEntry{filename: %s, startOffset: %d}",
                filename, startOffset);
    }
}
