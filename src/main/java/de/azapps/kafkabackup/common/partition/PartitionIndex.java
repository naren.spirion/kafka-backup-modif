package de.azapps.kafkabackup.common.partition;

import de.azapps.kafkabackup.common.segment.SegmentIndex;
import de.azapps.kafkabackup.common.segment.SegmentIndexEntry;
import de.azapps.kafkabackup.common.segment.SegmentUtils;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PartitionIndex {
    private static final byte V1_MAGIC_BYTE = 0x01;
    private Path indexFile;
    private List<PartitionIndexEntry> index = new ArrayList<>();
    private FileOutputStream fileOutputStream;
    private FileInputStream fileInputStream;
    private int position = 0;
    private long latestStartOffset = -1;

    // Bucket name & Key path & S3 Client.
    private Path partitionIndexKeyFile;
    private String bucketName;
    private S3Client s3Client;

    // Logging in this class too.
    private Logger log;

    // Regular constructor.
    public PartitionIndex(Path indexFile) throws IOException, IndexException {
        this.indexFile = indexFile;
        initFile();
        while (true) {
            try {
                PartitionIndexEntry partitionIndexEntry = PartitionIndexEntry.fromStream(fileInputStream);
                if (partitionIndexEntry.startOffset() <= latestStartOffset) {
                    throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index " + indexFile + "! Got " + partitionIndexEntry.startOffset() + " expected an offset larger than " + latestStartOffset);
                }
                index.add(partitionIndexEntry);
                latestStartOffset = partitionIndexEntry.startOffset();
            } catch (EOFException e) {
                // reached End of File
                break;
            }
        }
    }

    // Checks if the key exists in the bucket.
    public boolean keyExists(String bucket, String key) {
        try {
            HeadObjectResponse headResponse = s3Client
                    .headObject(
                            HeadObjectRequest
                                    .builder()
                                    .bucket(bucket)
                                    .key(key)
                                    .build()
                    );
            return true;
        } catch (NoSuchKeyException e) {
            return false;
        }
    }

    // Download the object to the file.
    private void downloadObject(String bucket, String key, Path indexFile) throws IndexException {
        try {
            GetObjectRequest request = GetObjectRequest
                    .builder()
                    .bucket(bucket)
                    .key(key)
                    .build();
            s3Client.getObject(request, indexFile);
        } catch (Exception e) {
            throw new IndexException("Failed to download the index file " + indexFile + " from bucket " + bucket + " at key path " + key + ".");
        }
    }

    // Enhanced constructor.
    public PartitionIndex(
            Path indexFile,
            Path partitionIndexKeyFile,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, IndexException {
        this.indexFile = indexFile;
        this.partitionIndexKeyFile = partitionIndexKeyFile;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        // Check if the file exists as an object on the S3 bucket.
        // Download to the path and use that if it exists, create a new file if that's not the case.

        // Checking if the bucket has the index file, download it if so.
        Files.deleteIfExists(indexFile);
        if (keyExists(bucketName, partitionIndexKeyFile.toString())) {
            log.info("Utilising " + partitionIndexKeyFile + " already present in S3 bucket.");
            downloadObject(bucketName, partitionIndexKeyFile.toString(), indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile(), true);
        } else {
            // Need to create the index file locally.
            log.info("Creating " + partitionIndexKeyFile + " locally.");
            Files.createFile(indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile());
            fileOutputStream.write(V1_MAGIC_BYTE);
        }

        this.fileInputStream = new FileInputStream(indexFile.toFile());
        fileInputStream.getChannel().position(0);
        byte[] v1Validation = new byte[1];
        if (fileInputStream.read(v1Validation) != 1 || v1Validation[0] != V1_MAGIC_BYTE) {
            throw new IndexException("Cannot validate Magic Byte in the beginning of the index " + indexFile);
        }

        while (true) {
            try {
                PartitionIndexEntry partitionIndexEntry = PartitionIndexEntry.fromStream(fileInputStream);
                if (partitionIndexEntry.startOffset() <= latestStartOffset) {
                    throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index " + indexFile + "! Got " + partitionIndexEntry.startOffset() + " expected an offset larger than " + latestStartOffset);
                }
                index.add(partitionIndexEntry);
                latestStartOffset = partitionIndexEntry.startOffset();
            } catch (EOFException e) {
                // reached End of File
                break;
            }
        }
    }
    private void initFile() throws IOException, IndexException {
        if (!Files.isRegularFile(indexFile)) {
            Files.createFile(indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile());
            fileOutputStream.write(V1_MAGIC_BYTE);
        } else {
            fileOutputStream = new FileOutputStream(indexFile.toFile(), true);
        }
        this.fileInputStream = new FileInputStream(indexFile.toFile());
        fileInputStream.getChannel().position(0);
        byte[] v1Validation = new byte[1];
        if (fileInputStream.read(v1Validation) != 1 || v1Validation[0] != V1_MAGIC_BYTE) {
            throw new IndexException("Cannot validate Magic Byte in the beginning of the index " + indexFile);
        }
    }

    public void setFinalOffset(long offset) {
        // Truncate the index till the last segment file having a starting offset <= this final offset.
        while (!index.isEmpty() && index.get(index.size() - 1).startOffset() > offset) {
            index.remove(index.size() - 1);
            if (!index.isEmpty()) {
                latestStartOffset = index.get(index.size() - 1).startOffset();
            } else {
                latestStartOffset = -1;
            }
        }
    }

    // ONLY FOR S3 BACKUPS: When segment files and record files are deleted,
    // the partition index needs to be updated correspondingly.
    public void truncatePartitionIndex() {
        if (bucketName == null) {
            return;
        }
        if (!index.isEmpty()) {
            List<PartitionIndexEntry> newIndex = new ArrayList<>();
            latestStartOffset = -1;
            boolean exists = false;
            for (PartitionIndexEntry entry : index) {
                if (exists) {
                    newIndex.add(entry);
                    continue;
                }
                String segmentFileName = entry.filename().concat("_records");
                String topic = partitionIndexKeyFile.toString().split("/")[0];
                Path segmentFileS3Path = Paths.get(topic, segmentFileName);
                log.info("Verifying the existence of {} for partition index reconstruction."
                        , segmentFileS3Path);

                if (keyExists(bucketName, segmentFileS3Path.toString())) {
                    newIndex.add(entry);
                    exists = true;
                    latestStartOffset = entry.startOffset();
                }
            }
            position = 0;
            index = newIndex;
        }
    }

    // Uploads the current local file to S3 at the path given by partitionIndexKeyFile.
    public void uploadToS3() {
        if (bucketName == null) {
            return;
        }
        try {
            PutObjectRequest request = PutObjectRequest
                    .builder()
                    .bucket(bucketName)
                    .key(partitionIndexKeyFile.toString())
                    .build();
            s3Client.putObject(request, indexFile);
            log.info("Backed up " + partitionIndexKeyFile.toString() + " to S3.");
        } catch (Exception e) {
            log.info("Error in backing up " + partitionIndexKeyFile.toString() + " to S3.");
        }
    }

    // Rewrites the partition index file. Preferably called after truncatePartitionIndex().
    public void rewriteFile() throws IOException {
        fileOutputStream.close();
        Files.deleteIfExists(indexFile);

        Files.createFile(indexFile);
        fileOutputStream = new FileOutputStream(indexFile.toFile());
        fileOutputStream.write(V1_MAGIC_BYTE);

        for (PartitionIndexEntry entry : index) {
            new PartitionIndexEntry(fileOutputStream, entry.filename(), entry.startOffset(), entry.startTimestamp());
        }
        log.info("File {} has been rewritten.", indexFile);
    }

    void appendSegment(String segmentFile, long startOffset, long startTimestamp) throws IOException, IndexException {
        if (startOffset <= latestStartOffset) {
            throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index " + indexFile + "! Got " + startOffset + " expected an offset larger than " + latestStartOffset);
        }
        PartitionIndexEntry indexEntry = new PartitionIndexEntry(fileOutputStream, segmentFile, startOffset, startTimestamp);
        index.add(indexEntry);
        latestStartOffset = startOffset;
    }

    Optional<PartitionIndexEntry> latestSegmentFile() {
        if (index.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(index.get(index.size() - 1));
        }
    }

    private Path deduceSegmentIndexLocation(Path indexFile, long startOffset, int partitionNumber) {
        String initialPath = indexFile.toString();
        initialPath = initialPath.substring(0, initialPath.length() - 20);
        return SegmentUtils.indexFile(Paths.get(initialPath), partitionNumber, startOffset);
    }

    public long getLastBackedUpOffset() {
        try {
            Optional<PartitionIndexEntry> lastEntry = latestSegmentFile();
            long result = -1;
            if (lastEntry.isPresent()) {
                long startOffset = lastEntry.get().startOffset();
                SegmentIndex segmentIndex;
                int partitionNumber = PartitionUtils.isPartitionIndex(indexFile).get();

                Path segmentIndexLocalPath = deduceSegmentIndexLocation(indexFile, startOffset, partitionNumber);
                if (bucketName != null) {
                    Path segmentIndexS3Path = deduceSegmentIndexLocation(partitionIndexKeyFile, startOffset, partitionNumber);
                    log.info("Downloading latest segment index for topic {} partition {} to determine offsets to restore until.");
                    segmentIndex = new SegmentIndex(
                            segmentIndexLocalPath,
                            segmentIndexS3Path,
                            bucketName,
                            s3Client,
                            log
                    );
                } else {
                    segmentIndex = new SegmentIndex(segmentIndexLocalPath);
                }
                Optional<SegmentIndexEntry> lastRecord = segmentIndex.lastIndexEntry();
                if (lastRecord.isPresent()) {
                    result = lastRecord.get().getOffset();
                } else { // Take the offset just before the starting offset of the index.
                    result = startOffset - 1;
                }
                segmentIndex.closeRestore();
            } else {
                if (bucketName != null) {
                    log.info("Empty Partition Index!");
                }
            }
            return result;
        } catch (Exception e) {
            return -1;
        }
    }

    long latestStartOffset() {
        return latestStartOffset;
    }

    void close() throws IOException {
        fileInputStream.close();
        fileOutputStream.close();
        if (bucketName != null) {
            try {
                PutObjectRequest request = PutObjectRequest
                        .builder()
                        .bucket(bucketName)
                        .key(partitionIndexKeyFile.toString())
                        .build();
                s3Client.putObject(request, indexFile);
                log.info("Backed up " + partitionIndexKeyFile.toString() + " to S3.");
            } catch (Exception e) {
                log.info("Error in backing up " + partitionIndexKeyFile.toString() + " to S3.");
            }
            // Delete the index file when closing.
            Files.deleteIfExists(indexFile);
        }
    }

    // Do not need to back up partition index during restoration.
    void closeRestore() throws IOException {
        fileInputStream.close();
        fileOutputStream.close();
        // Delete the index file when closing.
        if (bucketName != null) {
            Files.deleteIfExists(indexFile);
        }
    }

    void flush() throws IOException {
        fileOutputStream.flush();
    }

    long firstOffset() throws IndexException {
        if (index.size() == 0) {
            throw new PartitionIndex.IndexException("Partition Index is empty. Something is wrong with your partition index. Try to rebuild the index " + indexFile);
        }
        return index.get(0).startOffset();
    }

    // Sets the position instance variable to point to the first segment file
    // that may have records with this timestamp.
    void seekTimestamp(long timestamp) {
        int previousPosition = -1;
        // Iterate the index after the last element.
        for (int i = 0; i <= index.size(); i++) {
            if (i == index.size()) {
                position = previousPosition;
            } else {
                PartitionIndexEntry current = index.get(i);
                if (current.startTimestamp() > timestamp) {
                    position = Math.max(previousPosition, 0);
                    return;
                } else {
                    previousPosition = i;
                }
            }
        }
    }

    void seek(long offset) throws PartitionIndex.IndexException {
        int previousPosition = -1;
        // Iterate the index after the last element
        // Such that we can seek to an offset in the last index entry
        for (int i = 0; i <= index.size(); i++) {
            if (i == index.size()) {
                // Offset must be in the last index entry
                position = previousPosition;
            } else {
                PartitionIndexEntry current = index.get(i);
                if (current.startOffset() > offset) {
                    if (previousPosition >= 0) {
                        position = previousPosition;
                        //
                        return;
                    } else {
                        // Just return the first position here instead.
                        position = 0;
                        // throw new PartitionIndex.IndexException("No Index file found matching the target offset in partition index " + indexFile + ". Search for offset " + offset + ", smallest offset in index: " + current.startOffset());
                    }
                } else {
                    previousPosition = i;
                }
            }
        }
    }

    boolean hasMoreData() {
        return position < index.size();
    }

    String readFileName() {
        String fileName = index.get(position).filename();
        position++;
        // allow the cursor to be one after the index size.
        // This way we can detect easier when we reached the end of the index
        if (position > index.size()) {
            throw new IndexOutOfBoundsException("Index " + indexFile + " out of bound");
        }
        return fileName;
    }

    public List<PartitionIndexEntry> index() {
        return index;
    }

    public static class IndexException extends Exception {
        IndexException(String message) {
            super(message);
        }
    }
}
