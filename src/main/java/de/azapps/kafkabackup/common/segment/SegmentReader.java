package de.azapps.kafkabackup.common.segment;

import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.record.RecordSerde;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SegmentReader {
    private final String topic;
    private final int partition;
    private final String filePrefix;
    private final SegmentIndex segmentIndex;
    private final FileInputStream recordInputStream;

    // This needs to be updated in case we need to truncate the reading.
    private long lastValidStartPosition;

    private Path recordFile;

    // Bucket name & S3 Client.
    private String bucketName;
    private S3Client s3Client;

    // Logger
    private Logger log;

    // Final offset.
    private long finalOffset;

    public SegmentReader(String topic, int partition, Path topicDir, long startOffset) throws IOException, SegmentIndex.IndexException {
        this(topic, partition, topicDir, SegmentUtils.filePrefix(partition, startOffset));
    }

    // Regular constructor.
    public SegmentReader(String topic, int partition, Path topicDir, String filePrefix) throws IOException, SegmentIndex.IndexException {
        this.topic = topic;
        this.partition = partition;
        this.filePrefix = filePrefix;

        Path indexFile = SegmentUtils.indexFile(topicDir, filePrefix);
        Path recordFile = SegmentUtils.recordsFile(topicDir, filePrefix);
        if (!Files.isRegularFile(indexFile)) {
            throw new RuntimeException("Index for Segment not found: " + indexFile.toString());
        }
        if (!Files.isRegularFile(recordFile)) {
            throw new RuntimeException("Segment not found: " + recordFile.toString());
        }
        segmentIndex = new SegmentIndex(indexFile);
        recordInputStream = new FileInputStream(recordFile.toFile());
        SegmentUtils.ensureValidSegment(recordInputStream);
        lastValidStartPosition = segmentIndex.lastValidStartPosition();
    }

    // Checks if the key exists in the bucket.
    private boolean keyExists(String bucket, String key) {
        try {
            HeadObjectResponse headResponse = s3Client
                    .headObject(
                            HeadObjectRequest
                                    .builder()
                                    .bucket(bucket)
                                    .key(key)
                                    .build()
                    );
            return true;
        } catch (NoSuchKeyException e) {
            return false;
        }
    }

    // Download the object to the file.
    private void downloadObject(String bucket, String key, Path recordFile) throws SegmentReader.SegmentException {
        try {
            GetObjectRequest request = GetObjectRequest
                    .builder()
                    .bucket(bucket)
                    .key(key)
                    .build();
            s3Client.getObject(request, recordFile);
        } catch (Exception e) {
            throw new SegmentReader.SegmentException("Failed to download the records file " + recordFile + " from bucket " + bucket + " at key path " + key + ".");
        }
    }

    // Enhanced constructor.
    public SegmentReader(
            String topic,
            int partition,
            Path tempDir,
            String filePrefix,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, SegmentIndex.IndexException, SegmentReader.SegmentException {
        this.topic = topic;
        this.partition = partition;
        this.filePrefix = filePrefix;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        // This is where the actual files will be downloaded to locally from S3.
        Path indexFile = SegmentUtils.indexFile(tempDir, filePrefix);
        Path recordFile = SegmentUtils.recordsFile(tempDir, filePrefix);

        this.recordFile = recordFile;

        // Delete the segment records file if it already exists (download will fail otherwise).
        Files.deleteIfExists(recordFile);

        // Path to the segment records file in the S3 bucket.
        Path recordKeyFile = SegmentUtils.recordsFile(Paths.get(topic), filePrefix);
        if (!keyExists(bucketName, recordKeyFile.toString())) {
            throw new RuntimeException("Records for Segment not found in S3 bucket: " + recordKeyFile);
        }

        downloadObject(bucketName, recordKeyFile.toString(), recordFile);

        // This is where the segment index file is present in the S3 bucket.
        Path segmentIndexKeyFile = SegmentUtils.indexFile(Paths.get(topic), filePrefix);
        if (!keyExists(bucketName, segmentIndexKeyFile.toString())) {
            throw new RuntimeException("Index for Segment not found in S3 bucket: " + segmentIndexKeyFile);
        }

        // Using the enhanced SegmentIndex constructor.
        segmentIndex = new SegmentIndex(
                indexFile,
                segmentIndexKeyFile,
                bucketName,
                s3Client,
                log
        );

        recordInputStream = new FileInputStream(recordFile.toFile());
        SegmentUtils.ensureValidSegment(recordInputStream);
        lastValidStartPosition = segmentIndex.lastValidStartPosition();
    }

    public long findOffsetFromTimestamp(long timestamp) {
        return segmentIndex.findEarliestWithHigherOrEqualTimestamp(timestamp);
    }

    public void seek(long offset) throws IOException {
        Optional<Long> optionalPosition = segmentIndex.findEarliestWithHigherOrEqualOffset(offset);
        if (optionalPosition.isPresent()) {
            recordInputStream.getChannel().position(optionalPosition.get());
        } else {
            // If we couldn't find such a record, skip to EOF. This will make sure that hasMoreData() returns false.
            FileChannel fileChannel = recordInputStream.getChannel();
            fileChannel.position(fileChannel.size());
        }
    }

    public void setFinalOffset(long offset) {
        this.finalOffset = offset;
        segmentIndex.setFinalOffset(offset);
        this.lastValidStartPosition = segmentIndex.lastValidStartPosition();
    }

    public boolean hasMoreData() throws IOException {
        return recordInputStream.getChannel().position() <= lastValidStartPosition;
    }

    public Record read() throws IOException {
        if (!hasMoreData()) {
            throw new EOFException("Already read the last valid record in topic " + topic + ", segment " + filePrefix);
        }
        return RecordSerde.read(topic, partition, recordInputStream);
    }

    public List<Record> readN(int n) throws IOException {
        List<Record> records = new ArrayList<>(n);
        while (hasMoreData() && records.size() < n) {
            Record record = read();
            records.add(record);
        }
        return records;
    }

    public List<Record> readFully() throws IOException {
        List<Record> records = new ArrayList<>(segmentIndex.size());
        while (hasMoreData()) {
            Record record = read();
            records.add(record);
        }
        return records;
    }

    public void close() throws IOException {
        recordInputStream.close();
        // Deleting the file locally if restoring from S3 (since it's no longer needed).
        if (bucketName != null) {
            Files.deleteIfExists(recordFile);
        }
        segmentIndex.closeRestore();
    }

    public static class SegmentException extends Exception {
        SegmentException(String message) {
            super(message);
        }
    }
}
