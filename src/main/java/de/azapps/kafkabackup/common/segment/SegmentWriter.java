package de.azapps.kafkabackup.common.segment;

import de.azapps.kafkabackup.common.partition.PartitionIndex;
import de.azapps.kafkabackup.common.record.Record;
import de.azapps.kafkabackup.common.record.RecordSerde;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class SegmentWriter {
    private final String topic;
    private final int partition;
    private final long startOffset;
    private final SegmentIndex segmentIndex;
    private final FileOutputStream recordOutputStream;

    // Bucket name & S3 Client.
    private String bucketName;
    private S3Client s3Client;

    // Path to the record file needs to be an instance variable in the class to push it?
    private Path recordFile;
    private Path recordKeyFile;

    // Logging in this class too.
    private Logger log;

    // Regular constructor.
    public SegmentWriter(String topic, int partition, long startOffset, Path topicDir) throws IOException, SegmentIndex.IndexException {
        this.topic = topic;
        this.partition = partition;
        this.startOffset = startOffset;

        Path indexFile = SegmentUtils.indexFile(topicDir, partition, startOffset);
        segmentIndex = new SegmentIndex(indexFile);

        Path recordFile = SegmentUtils.recordsFile(topicDir, partition, startOffset);
        if (!Files.isRegularFile(recordFile)) {
            Files.createFile(recordFile);
            recordOutputStream = new FileOutputStream(recordFile.toFile());
            recordOutputStream.write(SegmentUtils.V1_MAGIC_BYTE);
        } else {
            // Validate Magic Byte
            FileInputStream inputStream = new FileInputStream(recordFile.toFile());
            SegmentUtils.ensureValidSegment(inputStream);
            inputStream.close();

            // move to last committed position of the file
            recordOutputStream = new FileOutputStream(recordFile.toFile(), true);
            Optional<SegmentIndexEntry> optionalPreviousIndexEntry = segmentIndex.lastIndexEntry();
            if (optionalPreviousIndexEntry.isPresent()) {
                SegmentIndexEntry previousSegmentIndexEntry = optionalPreviousIndexEntry.get();
                long position = previousSegmentIndexEntry.recordFilePosition() + previousSegmentIndexEntry.recordByteLength();
                recordOutputStream.getChannel().position(position);
            } else {
                recordOutputStream.getChannel().position(1);
            }
        }
    }

    // Checks if the key exists in the bucket.
    private boolean keyExists(String bucket, String key) {
        try {
            HeadObjectResponse headResponse = s3Client
                    .headObject(
                            HeadObjectRequest
                                    .builder()
                                    .bucket(bucket)
                                    .key(key)
                                    .build()
                    );
            return true;
        } catch (NoSuchKeyException e) {
            return false;
        }
    }

    // Download the object to the file.
    private void downloadObject(String bucket, String key, Path indexFile) throws SegmentException {
        try {
            GetObjectRequest request = GetObjectRequest
                    .builder()
                    .bucket(bucket)
                    .key(key)
                    .build();
            s3Client.getObject(request, indexFile);
        } catch (Exception e) {
            throw new SegmentException("Failed to download the records file " + indexFile + " from bucket " + bucket + " at key path " + key + ".");
        }
    }

    // Enhanced constructor.
    public SegmentWriter(
            String topic,
            int partition,
            long startOffset,
            Path topicDir,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, SegmentIndex.IndexException, SegmentException {
        this.topic = topic;
        this.partition = partition;
        this.startOffset = startOffset;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        Path indexFile = SegmentUtils.indexFile(topicDir, partition, startOffset);
        // Path in the S3 Bucket to the segment index.
        Path segmentIndexKeyFile = SegmentUtils.indexFile(Paths.get(topic), partition, startOffset);

        segmentIndex = new SegmentIndex(indexFile, segmentIndexKeyFile, bucketName, s3Client, log);

        Path recordFile = SegmentUtils.recordsFile(topicDir, partition, startOffset);
        // Path in the S3 Bucket to the segment records file.
        Path recordKeyFile = SegmentUtils.recordsFile(Paths.get(topic), partition, startOffset);

        this.recordFile = recordFile;
        this.recordKeyFile = recordKeyFile;

        // Check if the file exists as an object on the S3 bucket.
        // Download to the path and use that if it exists, create a new file if that's not the case.

        // if (!Files.isRegularFile(recordFile)) {
            Files.deleteIfExists(recordFile);
            if (!keyExists(bucketName, recordKeyFile.toString())) {
                log.info("Creating " + recordKeyFile + " locally.");
                Files.createFile(recordFile);
                recordOutputStream = new FileOutputStream(recordFile.toFile());
                recordOutputStream.write(SegmentUtils.V1_MAGIC_BYTE);
                return;
            } else {
                log.info("Utilising " + recordKeyFile + " already present in S3 bucket.");
                downloadObject(bucketName, recordKeyFile.toString(), recordFile);
            }
        // }
        // Validate Magic Byte
        FileInputStream inputStream = new FileInputStream(recordFile.toFile());
        SegmentUtils.ensureValidSegment(inputStream);
        inputStream.close();

        // move to last committed position of the file
        recordOutputStream = new FileOutputStream(recordFile.toFile(), true);
        Optional<SegmentIndexEntry> optionalPreviousIndexEntry = segmentIndex.lastIndexEntry();
        if (optionalPreviousIndexEntry.isPresent()) {
            SegmentIndexEntry previousSegmentIndexEntry = optionalPreviousIndexEntry.get();
            long position = previousSegmentIndexEntry.recordFilePosition() + previousSegmentIndexEntry.recordByteLength();
            recordOutputStream.getChannel().position(position);
        } else {
            recordOutputStream.getChannel().position(1);
        }
    }

    public void setLastWrittenTimestamp(long timestamp) {
        segmentIndex.setLatestValidRecordTimestamp(timestamp);
    }

    public long lastWrittenOffset() {
        return segmentIndex.lastIndexEntry().map(SegmentIndexEntry::getOffset).orElse(-1L);
    }

    public long latestWrittenTimestamp() {
        return segmentIndex.getLatestValidRecordTimestamp();
    }

    public void append(Record record) throws IOException, SegmentIndex.IndexException, SegmentException {
        if (!record.topic().equals(topic)) {
            throw new SegmentException("Trying to append to wrong topic!\n" +
                    "Expected topic: " + topic + " given topic: " + record.topic());
        }
        if (record.kafkaPartition() != partition) {
            throw new SegmentException("Trying to append to wrong partition!\n" +
                    "Expected partition: " + partition + " given partition: " + partition);
        }
        if (record.kafkaOffset() < startOffset) {
            throw new SegmentException("Try to append a record with an offset smaller than the start offset. Something is very wrong. \n" +
                    "Topic: " + record.topic() + "Partition: " + record.kafkaPartition() + " StartOffset: " + startOffset + " RecordOffset: " + record.kafkaOffset() + "\n" +
                    "You probably forgot to delete a previous Backup\n");
        }
        if (record.kafkaOffset() <= lastWrittenOffset()) {
            // We are handling the offsets ourselves. This should never happen!
            throw new SegmentException("Trying to override a written record. There is something terribly wrong in your setup! Please check whether you are trying to override an existing backup" +
                    "Topic: " + record.topic() + "Partition: " + record.kafkaPartition() + " lastWrittenOffset: " + lastWrittenOffset() + " RecordOffset: " + record.kafkaOffset());
        }
        long startPosition = recordOutputStream.getChannel().position();
        RecordSerde.write(recordOutputStream, record);
        long recordByteLength = recordOutputStream.getChannel().position() - startPosition;
        SegmentIndexEntry segmentIndexEntry = new SegmentIndexEntry(record.kafkaOffset(), record.timestamp(), startPosition, recordByteLength);
        segmentIndex.addEntry(segmentIndexEntry);
    }

    public String filePrefix() {
        return SegmentUtils.filePrefix(partition, startOffset);
    }

    public long size() throws IOException {
        return recordOutputStream.getChannel().size();
    }

    public void flush() throws IOException {
        recordOutputStream.flush();
        segmentIndex.flush();
    }

    public void close() throws IOException {
        recordOutputStream.close();
        segmentIndex.close();
        if (bucketName != null) {
            try {
                PutObjectRequest request = PutObjectRequest
                        .builder()
                        .bucket(bucketName)
                        .key(recordKeyFile.toString())
                        .build();
                s3Client.putObject(request, recordFile);
                log.info("Backed up " + recordKeyFile.toString() + " to S3.");
            } catch (Exception e) {
                log.info("Error in backing up " + recordKeyFile.toString() + "to S3.");
            }
            Files.deleteIfExists(recordFile);
        }
    }

    public static class SegmentException extends Exception {
        SegmentException(String message) {
            super(message);
        }
    }
}
