package de.azapps.kafkabackup.common.segment;

import de.azapps.kafkabackup.common.partition.PartitionIndex;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SegmentIndex {
    private static final byte V1_MAGIC_BYTE = 0x01;
    private Path indexFile;
    private List<SegmentIndexEntry> index = new ArrayList<>();
    private long lastValidRecordOffset = -1;
    private long lastValidIndexPosition = 1; // mind the magic byte!
    private long latestValidRecordTimestamp = -1;
    private FileOutputStream fileOutputStream;
    private FileInputStream fileInputStream;

    // Bucket name & Key path & S3 Client.
    private Path segmentIndexKeyFile;
    private String bucketName;
    private S3Client s3Client;

    // Logging for this class too.
    private Logger log;

    // Regular constructor.
    public SegmentIndex(Path indexFile) throws IOException, IndexException {
        this.indexFile = indexFile;
        initFile();
        while (true) {
            try {
                SegmentIndexEntry segmentIndexEntry = SegmentIndexEntry.fromStream(fileInputStream);
                if (segmentIndexEntry.getOffset() <= lastValidRecordOffset) {
                    throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index!");
                }
                if (segmentIndexEntry.recordTimeStamp() < latestValidRecordTimestamp) {
                    throw new IndexException("Timestamps must be increasing (not necessarily strictly)! There is something terribly wrong in your index!");
                }
                index.add(segmentIndexEntry);
                lastValidRecordOffset = segmentIndexEntry.getOffset();
                latestValidRecordTimestamp = segmentIndexEntry.recordTimeStamp();
                lastValidIndexPosition = fileInputStream.getChannel().position();
            } catch (EOFException e) {
                // reached End of File
                break;
            }
        }
    }

    // Checks if the key exists in the bucket.
    private boolean keyExists(String bucket, String key) {
        try {
            HeadObjectResponse headResponse = s3Client
                    .headObject(
                            HeadObjectRequest
                                    .builder()
                                    .bucket(bucket)
                                    .key(key)
                                    .build()
                    );
            return true;
        } catch (NoSuchKeyException e) {
            return false;
        }
    }

    // Download the object to the file.
    private void downloadObject(String bucket, String key, Path indexFile) throws IndexException {
        try {
            GetObjectRequest request = GetObjectRequest
                    .builder()
                    .bucket(bucket)
                    .key(key)
                    .build();
            s3Client.getObject(request, indexFile);
        } catch (Exception e) {
            throw new IndexException("Failed to download the index file " + indexFile + " from bucket " + bucket + " at key path " + key + ".");
        }
    }

    // Enhanced constructor.
    public SegmentIndex(
            Path indexFile,
            Path segmentIndexKeyFile,
            String bucketName,
            S3Client s3Client,
            Logger log
    ) throws IOException, IndexException {
        this.indexFile = indexFile;
        this.segmentIndexKeyFile = segmentIndexKeyFile;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
        this.log = log;

        // Check if the file exists as an object on the S3 bucket.
        // Download to the path and use that if it exists, create a new file if that's not the case.

        // Checking if the bucket contains the index file, download it if so.
        Files.deleteIfExists(indexFile);
        if (keyExists(bucketName, segmentIndexKeyFile.toString())) {
            log.info("Utilising " + segmentIndexKeyFile + " already present in S3 bucket.");
            downloadObject(bucketName, segmentIndexKeyFile.toString(), indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile(), true);
        } else {
            // Need to create the index file locally.
            log.info("Creating " + segmentIndexKeyFile + " locally.");
            Files.createFile(indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile());
            fileOutputStream.write(V1_MAGIC_BYTE);
        }

        this.fileInputStream = new FileInputStream(indexFile.toFile());
        byte[] v1Validation = new byte[1];
        if (fileInputStream.read(v1Validation) != 1 || v1Validation[0] != V1_MAGIC_BYTE) {
            throw new IndexException("Cannot validate Magic Byte in the beginning of the index " + indexFile);
        }
        while (true) {
            try {
                SegmentIndexEntry segmentIndexEntry = SegmentIndexEntry.fromStream(fileInputStream);
                if (segmentIndexEntry.getOffset() <= lastValidRecordOffset) {
                    throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index!");
                }
                if (segmentIndexEntry.recordTimeStamp() < latestValidRecordTimestamp) {
                    throw new IndexException("Timestamps must be increasing (not necessarily strictly)! There is something terribly wrong in your index!");
                }
                index.add(segmentIndexEntry);
                lastValidRecordOffset = segmentIndexEntry.getOffset();
                latestValidRecordTimestamp = segmentIndexEntry.recordTimeStamp();
                lastValidIndexPosition = fileInputStream.getChannel().position();
            } catch (EOFException e) {
                // reached End of File
                break;
            }
        }
    }

    private void initFile() throws IOException, IndexException {
        if (!Files.isRegularFile(indexFile)) {
            Files.createFile(indexFile);
            fileOutputStream = new FileOutputStream(indexFile.toFile());
            fileOutputStream.write(V1_MAGIC_BYTE);
        } else {
            fileOutputStream = new FileOutputStream(indexFile.toFile(), true);
        }
        this.fileInputStream = new FileInputStream(indexFile.toFile());
        byte[] v1Validation = new byte[1];
        if (fileInputStream.read(v1Validation) != 1 || v1Validation[0] != V1_MAGIC_BYTE) {
            throw new IndexException("Cannot validate Magic Byte in the beginning of the index " + indexFile);
        }
    }

    void addEntry(SegmentIndexEntry segmentIndexEntry) throws IOException, IndexException {
        if (segmentIndexEntry.getOffset() <= lastValidRecordOffset) {
            throw new IndexException("Offsets must be always increasing! There is something terribly wrong in your index!");
        }
        // Timestamps in the index should be monotonically increasing.
        long currentTimeStamp = segmentIndexEntry.recordTimeStamp();
        if (currentTimeStamp < latestValidRecordTimestamp) {
            segmentIndexEntry.setRecordTimeStamp(latestValidRecordTimestamp);
        }

        fileOutputStream.getChannel().position(lastValidIndexPosition);
        segmentIndexEntry.writeToStream(fileOutputStream);
        lastValidIndexPosition = fileOutputStream.getChannel().position();
        lastValidRecordOffset = segmentIndexEntry.getOffset();
        latestValidRecordTimestamp = segmentIndexEntry.recordTimeStamp();
        index.add(segmentIndexEntry);
    }

    public void setFinalOffset(long offset) {
        // Truncate the index till the last segment file having a starting offset <= this final offset.
        while (!index.isEmpty() && index.get(index.size() - 1).getOffset() > offset) {
            index.remove(index.size() - 1);
        }
    }

    public Optional<SegmentIndexEntry> lastIndexEntry() {
        if (!index.isEmpty()) {
            return Optional.of(index.get(index.size() - 1));
        } else {
            return Optional.empty();
        }
    }

    long lastValidStartPosition() {
        if (!index.isEmpty()) {
            return index.get(index.size() - 1).recordFilePosition();
        } else {
            return 0L;
        }

    }

    Optional<SegmentIndexEntry> getByPosition(int position) {
        if (position >= index.size()) {
            return Optional.empty();
        } else {
            return Optional.of(index.get(position));
        }
    }

    public void setLatestValidRecordTimestamp(long timestamp) {
        this.latestValidRecordTimestamp = Math.max(latestValidRecordTimestamp, timestamp);
    }

    public long getLatestValidRecordTimestamp() {
        return this.latestValidRecordTimestamp;
    }

    Optional<Long> findByOffset(long offset) {
        for (SegmentIndexEntry current : index) {
            if (current.getOffset() == offset) {
                return Optional.of(current.recordFilePosition());
            }
        }
        return Optional.empty();
    }

    long findEarliestWithHigherOrEqualTimestamp(long timestamp) {
        if (timestamp > latestValidRecordTimestamp) {
            // By default, a segment index is empty only if it's for the first segment in the partition.
            // So it should be okay to return 0 in that case.
            Optional<SegmentIndexEntry> finalEntry = lastIndexEntry();
            if (!finalEntry.isPresent()) {
                return 0;
            } else {
                return finalEntry.get().getOffset() + 1;
            }
        }
        for (SegmentIndexEntry current : index) {
            if (current.recordTimeStamp() >= timestamp) {
                return current.getOffset();
            }
        }
        // This should never be reached, just to appease the compiler.
        return 0;
    }

    Optional<Long> findEarliestWithHigherOrEqualOffset(long offset) {
        for (SegmentIndexEntry current : index) {
            if (current.getOffset() >= offset) {
                return Optional.of(current.recordFilePosition());
            }
        }
        return Optional.empty();
    }

    int size() {
        return index.size();
    }

    public List<SegmentIndexEntry> index() {
        return index;
    }

    void flush() throws IOException {
        fileOutputStream.flush();
    }

    void close() throws IOException {
        fileInputStream.close();
        fileOutputStream.close();
        if (bucketName != null) {
            try {
                PutObjectRequest request = PutObjectRequest
                        .builder()
                        .bucket(bucketName)
                        .key(segmentIndexKeyFile.toString())
                        .build();
                s3Client.putObject(request, indexFile);
                log.info("Backed up " + segmentIndexKeyFile.toString() + " to S3.");
            } catch (Exception e) {
                log.info("Error in backing up " + segmentIndexKeyFile.toString() + " to S3.");
            }
            Files.deleteIfExists(indexFile);
        }
    }

    // Do not need to back up segment index during restoration.
    public void closeRestore() throws IOException {
        fileInputStream.close();
        fileOutputStream.close();
        if (bucketName != null) {
            Files.deleteIfExists(indexFile);
        }
    }

    public static class IndexException extends Exception {
        IndexException(String message) {
            super(message);
        }
    }

}
