package de.azapps.kafkabackup.common.offset;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ConsumerGroupListing;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.errors.RetriableException;
import org.slf4j.Logger;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class OffsetSink {
    private final Path targetDir;
    private final Map<TopicPartition, OffsetStoreFile> topicOffsets = new HashMap<>();
    private List<String> consumerGroups = new ArrayList<>();
    private final AdminClient adminClient;

    // Logger, bucket name & S3 Client.
    private Logger log;
    private String bucketName;
    private S3Client s3Client;

    // Regular constructor.
    public OffsetSink(AdminClient adminClient, Path targetDir) {
        this.adminClient = adminClient;
        this.targetDir = targetDir;
    }

    // Enhanced constructor.
    public OffsetSink(
            AdminClient adminClient,
            Path targetDir,
            Logger log,
            String bucketName,
            S3Client s3Client
            ) {
        this.adminClient = adminClient;
        this.targetDir = targetDir;
        this.log = log;
        this.bucketName = bucketName;
        this.s3Client = s3Client;
    }

    public void syncConsumerGroups() {
        try {
            consumerGroups = adminClient.listConsumerGroups().all().get().stream().map(ConsumerGroupListing::groupId).collect(Collectors.toList());
        } catch (InterruptedException | ExecutionException e) {
            throw new RetriableException(e);
        }
    }

    public void syncOffsets() throws IOException {
        boolean error = false;
        for (String consumerGroup : consumerGroups) {
            try {
                syncOffsetsForGroup(consumerGroup);
            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            }
        }
        if (error) {
            throw new IOException("syncOffsets() threw an IOException");
        }
    }

    private void syncOffsetsForGroup(String consumerGroup) throws IOException {
        Map<TopicPartition, OffsetAndMetadata> topicOffsetsAndMetadata;
        try {
            topicOffsetsAndMetadata = adminClient.listConsumerGroupOffsets(consumerGroup).partitionsToOffsetAndMetadata().get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RetriableException(e);
        }
        for (Map.Entry<TopicPartition, OffsetAndMetadata> entry : topicOffsetsAndMetadata.entrySet()) {
            TopicPartition tp = entry.getKey();
            OffsetAndMetadata offsetAndMetadata = entry.getValue();

            if (validTopic(tp.topic())) {
                if (!this.topicOffsets.containsKey(tp)) {
                    if (bucketName == null) {
                        this.topicOffsets.put(tp, new OffsetStoreFile(targetDir, tp));
                    } else {
                        try {
                            this.topicOffsets.put(tp, new OffsetStoreFile(targetDir, tp, log, bucketName, s3Client));
                        } catch (OffsetStoreFile.OffsetException e) {
                            log.error("Unable to create an offset file for the topic partition: " + tp);
                        }
                    }
                }
                OffsetStoreFile offsets = this.topicOffsets.get(tp);
                offsets.put(consumerGroup, offsetAndMetadata.offset());
            }
        }
    }

    private boolean validTopic(String topic) {
        return Files.isDirectory(Paths.get(targetDir.toString(), topic));
    }

    public void flush() throws IOException {
        boolean error = false;
        for (OffsetStoreFile offsetStoreFile : topicOffsets.values()) {
            try {
                offsetStoreFile.flush();
            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            }
        }
        if (error) {
            throw new IOException("syncOffsets() threw an IOException");
        }
    }

    // Updating close() because I only want to write to S3 once, i.e. when the task stops.
    public void close() throws IOException {
        boolean error = false;
        for (OffsetStoreFile offsetStoreFile : topicOffsets.values()) {
            try {
                offsetStoreFile.close();
            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            }
        }
        if (error) {
            throw new IOException("syncOffsets() threw an IOException");
        }
    }

    private static class OffsetStoreFile {
        private Map<String, Long> groupOffsets = new HashMap<>();

        private final ObjectMapper mapper = new ObjectMapper();
        private final Path storeFile;

        // Logger, bucket name & S3 Client.
        private Logger log;
        private String bucketName;
        private S3Client s3Client;

        // Path in the S3 bucket.
        private Path storeKeyFile;

        // Regular constructor.
        OffsetStoreFile(Path targetDir, TopicPartition topicPartition) throws IOException {
            storeFile = OffsetUtils.offsetStoreFile(targetDir, topicPartition);
            if (!Files.isRegularFile(storeFile)) {
                Files.createFile(storeFile);
            }
            if (Files.size(storeFile) > 0) {
                groupOffsets = mapper.readValue(storeFile.toFile(), Map.class);
            }
        }

        // Checks if the key exists in the bucket.
        private boolean keyExists(String bucket, String key) {
            try {
                HeadObjectResponse headResponse = s3Client
                        .headObject(
                                HeadObjectRequest
                                        .builder()
                                        .bucket(bucket)
                                        .key(key)
                                        .build()
                        );
                return true;
            } catch (NoSuchKeyException e) {
                return false;
            }
        }

        // Download the object to the file.
        private void downloadObject(String bucket, String key, Path storeFile) throws OffsetException {
            try {
                GetObjectRequest request = GetObjectRequest
                        .builder()
                        .bucket(bucket)
                        .key(key)
                        .build();
                s3Client.getObject(request, storeFile);
            } catch (Exception e) {
                throw new OffsetException("Failed to download the offset store file " + storeFile + " from bucket " + bucket + " at key path " + key + ".");
            }
        }

        // Enhanced constructor.
        OffsetStoreFile(
                Path targetDir,
                TopicPartition topicPartition,
                Logger log,
                String bucketName,
                S3Client s3Client
        ) throws IOException, OffsetException {
            this.log = log;
            this.bucketName = bucketName;
            this.s3Client = s3Client;

            storeFile = OffsetUtils.offsetStoreFile(targetDir, topicPartition);
            storeKeyFile = OffsetUtils.offsetStoreFile(Paths.get(""), topicPartition);

//            if (!Files.isRegularFile(storeFile)) {
                Files.deleteIfExists(storeFile);
                // We need to check if the offset file is in S3.
                if (keyExists(bucketName, storeKeyFile.toString())) {
                    log.info("Utilising " + storeKeyFile + " stored in S3.");
                    downloadObject(bucketName, storeKeyFile.toString(), storeFile);
                } else {
                    log.info("Creating new " + storeKeyFile + " locally.");
                    Files.createFile(storeFile);
                }
//            }
            if (Files.size(storeFile) > 0) {
                groupOffsets = mapper.readValue(storeFile.toFile(), Map.class);
            }
        }

        void put(String consumerGroup, long offset) {
            groupOffsets.put(consumerGroup, offset);
        }

        void close() throws IOException {
            mapper.writeValue(storeFile.toFile(), groupOffsets);
            // Write to S3 here.
            if (bucketName != null) {
                try {
                    PutObjectRequest request = PutObjectRequest
                            .builder()
                            .bucket(bucketName)
                            .key(storeKeyFile.toString())
                            .build();
                    s3Client.putObject(request, storeFile);
                    log.info("Backed up " + storeKeyFile + " to S3.");
                } catch (Exception e) {
                    log.info("Error in backing up " + storeKeyFile + " to S3.");
                }
                // Deleting the store file.
                Files.deleteIfExists(storeFile);
            }
        }

        void flush() throws IOException {
            mapper.writeValue(storeFile.toFile(), groupOffsets);
        }

        public static class OffsetException extends Exception {
            OffsetException(String message) {
                super(message);
            }
        }
    }
}
