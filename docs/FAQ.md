# Consumer offsets not getting restored & CommitFailedExceptions.

> My consumer's offsets didn't get restored, and I see CommitFailedExceptions in the Kafka Connect logs!

This is one major limitation of the tool. Consumer offsets cannot be restored when the consumer groups in question are active.
All concerning consumer groups must be made inactive for proper consumer offset restoration.

This means that if the sink connector is run at the same time as the source connector, an exception will get thrown.
This is because the source tasks will attempt to (unsuccessfully) restore offsets of the sink connector.
which are stored just like any other consumer group in Kafka. Shouldn't be anything to worry about.

# Live data during restoration.

> My consumer misses most of the live data that came in during the restore process.

Yet another limitation of the tool. Because of the way Kafka Backup restores consumer offsets, you have a high chance of missing any live data
coming into the topics (that are being restored) during restoration.
No live data & inactive consumer groups represent the only ideal scenario where consumer offset restoration will work flawlessly.

# How to restore to a different topic

> I didn't see in the documentation if it's possible to be able to restore to a different destination topic, such as mybackupedtopic-restored. It would help with testing restore procedures without disturbing the existing topic, among other things.

Simply rename the topic directories in the Backup target. Same procedure applies in the case of an S3 restoration, currently.

# Restoring a multi-partition topic does not work

> When I restore topic with 24 partitions it creates topic with one partitions and restore failed.
> Restore successful if I create 24 partitions topic before restore. 

You need to create the topic manually before restore. For a "real" backup scenario you also need to backup and restore Zookeeper

# Error "Plugin class loader for connector was not found" 

```sh
ERROR Plugin class loader for connector: 'de.azapps.kafkabackup.sink.BackupSinkConnector' was not found. Returning: org.apache.kafka.connect.runtime.isolation.DelegatingClassLoader@5b068087 (org.apache.kafka.connect.runtime.isolation.DelegatingClassLoader:165)
```

You forgot to build the jar file. Either get an official release of Kafka Backup or run `./gradlew shadowJar` in the root directory of Kafka Backup.