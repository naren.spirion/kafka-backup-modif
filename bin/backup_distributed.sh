#!/bin/bash
# saner programming env: these switches turn some bugs into errors
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test >/dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
  echo '`getopt --test` failed in this environment.'
  exit 1
fi

WORKING_DIR="$(mktemp -d -t kafka-backup.XXXXXX)"

# Cleanup after SIGTERM/SIGINT
_term() {
  rm -r "$WORKING_DIR"
}

trap _term SIGTERM
trap _term INT

##################################### Parse arguments

OPTIONS="h"
LONGOPTS=bootstrap-server:,target-dir:,bucket-name:,topics:,topics-regex:,max-sink-tasks:,max-segment-size:,command-config:,help,debug,snapshot

HELP=$(
  cat <<END
--bootstrap-server  [REQUIRED] The Kafka server to connect to
--target-dir        [REQUIRED] Directory where the backup files should be stored
--bucket-name       [REQUIRED] AWS S3 Bucket where the backup files should be stored
--topics            <T1,T2,…>  List of topics to be backed up. You must provide either --topics or --topics-regex. Not both
--topics-regex                 Regex of topics to be backed up. You must provide either --topics or --topics-regex. Not both
--max-sink-tasks               The maximum number of backup sink tasks permitted. Default: 1.
                               [RECOMMENDED MAXIMUM: Equivalent to the number of topic partitions to be backed up.]
                               [IMPORTANT: Do not make this parameter > 1 if using --bucket-name null (i.e. backing up to the file system)
                               Your Kafka Connect cluster should also contain only a single distributed worker in that case.]
--max-segment-size             Size of the backup segments in bytes DEFAULT: 1GiB
--command-config    <FILE>     Property file containing configs to be
                               passed to Admin Client. Only useful if you have additional connection options
--help                         Prints this message
--debug                        Print Debug information (if using the environment variable, set it to 'y')
--snapshot                     One-off backup mode

You can also set all parameters using environment variables. Use CAPITAL LETTERS and underscores (_) instead of dashes (-).
E.g. BOOTSTRAP_SERVER instead of --bootstrap-server
END
)

# Allow to set parameters using environment variables

[ -z ${BOOTSTRAP_SERVER+x} ] && BOOTSTRAP_SERVER=""
[ -z ${TARGET_DIR+x} ] && TARGET_DIR=""
[ -z ${BUCKET_NAME+x} ] && BUCKET_NAME=""
[ -z ${TOPICS+x} ] && TOPICS=""
[ -z ${TOPICS_REGEX+x} ] && TOPICS_REGEX=""
[ -z ${MAX_SEGMENT_SIZE+x} ] && MAX_SEGMENT_SIZE="$((1 * 1024 * 1024 * 1024))" # 1GiB
[ -z ${COMMAND_CONFIG+x} ] && COMMAND_CONFIG=""
[ -z ${DEBUG+x} ] && DEBUG="n"
[ -z ${SNAPSHOT+x} ] && SNAPSHOT="false"
[ -z ${MAX_SINK_TASKS+x} ] && MAX_SINK_TASKS=1

PLUGIN_PATH="$(dirname "${BASH_SOURCE[0]}")"
CONNECT_BIN=""

# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # e.g. return value is 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
  case "$1" in
  --bootstrap-server)
    BOOTSTRAP_SERVER="$2"
    shift 2
    ;;
  --target-dir)
    TARGET_DIR="$2"
    shift 2
    ;;
  --bucket-name)
    BUCKET_NAME="$2"
    shift 2
    ;;
  --topics)
    TOPICS="$2"
    shift 2
    ;;
  --topics-regex)
    TOPICS_REGEX="$2"
    shift 2
    ;;
  --max-sink-tasks)
    MAX_SINK_TASKS="$2"
    shift 2
    ;;
  --max-segment-size)
    MAX_SEGMENT_SIZE="$2"
    shift 2
    ;;
  --command-config)
    COMMAND_CONFIG="$2"
    shift 2
    ;;
  -h | --help)
    echo "$HELP"
    exit 0
    ;;
  -d | --debug)
    DEBUG=y
    shift
    ;;
  -s | --snapshot)
    SNAPSHOT=true
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    echo "$1 $2 Programming error"
    exit 3
    ;;
  esac
done

##################################### Check Arguments

if [ -z "$BOOTSTRAP_SERVER" ]; then
  echo "--bootstrap-server is missing"
  echo "$HELP"
  exit 1
fi

if [ -z "$TARGET_DIR" ]; then
  echo "--target-dir is missing"
  echo "$HELP"
  exit 1
fi

if [ -z "$BUCKET_NAME" ]; then
  echo "--bucket-name is missing"
  echo "$HELP"
  exit 1
fi

if [ ! -d "$TARGET_DIR" ]; then
  mkdir "$TARGET_DIR"
fi

if { [ -z "$TOPICS" ] && [ -z "$TOPICS_REGEX" ]; } || { [ -n "$TOPICS" ] && [ -n "$TOPICS_REGEX" ]; }; then
  echo "You need to provide either --topics or --topics-regex not both nor none"
  echo "$HELP"
  exit 1
fi

if [ -n "$COMMAND_CONFIG" ] && [ ! -f "$COMMAND_CONFIG" ]; then
  echo "no such file $COMMAND_CONFIG"
  exit 1
fi

if { [ "$BUCKET_NAME" == "null" ] && [ "$MAX_SINK_TASKS" -gt 1 ]; }; then
  echo "Cannot backup to the filesystem with multiple sink tasks."
  echo "$HELP"
  exit 1
fi

if [ ! -f "$PLUGIN_PATH/kafka-backup.jar" ]; then
  echo "Cannot find the kafka-backup.jar in $PLUGIN_PATH. Please set --backup-jar accordingly"
  exit 1
fi

##################################### Connector Configuration

CONNECTOR_JSON='{
"name" : "backup-sink",
"connector.class" : "de.azapps.kafkabackup.sink.BackupSinkConnector",
"tasks.max" : "'"$MAX_SINK_TASKS"'",
"key.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"value.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"header.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"target.dir" : "'"$TARGET_DIR"'",
"bucket.name" : "'"$BUCKET_NAME"'",
"max.segment.size.bytes" : "'"$MAX_SEGMENT_SIZE"'",
"cluster.bootstrap.servers" : "'"$BOOTSTRAP_SERVER"'",
"snapshot" : "'"$SNAPSHOT"'",'

JSON_LOCATION="$WORKING_DIR/configuration.json"

echo "$CONNECTOR_JSON" >"$JSON_LOCATION"

if [ -n "$TOPICS" ]; then
  echo \"topics\" : \""$TOPICS"\" >>"$JSON_LOCATION"
fi

if [ -n "$TOPICS_REGEX" ]; then
  echo \"topics.regex\" : \""$TOPICS_REGEX"\" >>"$JSON_LOCATION"
fi

echo "}" >>"$JSON_LOCATION"

cat "$JSON_LOCATION"

# JSON file is sent to Kafka Connect via the REST API (contains the sink connector configurations).
curl -i -X PUT -H "Content-Type:application/json" http://localhost:8083/connectors/backup-sink/config -d @"$JSON_LOCATION"
echo "Distributed backup sink connector posted."
# Delete the working directory.
_term