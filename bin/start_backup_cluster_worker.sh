#!/bin/bash
# saner programming env: these switches turn some bugs into errors
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test >/dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
  echo '`getopt --test` failed in this environment.'
  exit 1
fi

WORKING_DIR="$(mktemp -d -t kafka-backup.XXXXXX)"

# Cleanup after SIGTERM/SIGINT
_term() {
  rm -r "$WORKING_DIR"
}

trap _term SIGTERM
trap _term INT

##################################### Parse arguments

OPTIONS="h"
LONGOPTS=bootstrap-server:,command-config:,help,debug

HELP=$(
  cat <<END
--bootstrap-server  [REQUIRED] The Kafka server to connect the worker to.
--command-config    <FILE>     Property file containing configs to be
                               passed to Admin Client. Only useful if you have additional connection options
--help                         Prints this message
--debug                        Print Debug information (if using the environment variable, set it to 'y')

You can also set all parameters using environment variables. Use CAPITAL LETTERS and underscores (_) instead of dashes (-).
E.g. BOOTSTRAP_SERVER instead of --bootstrap-server
END
)

# Allow to set parameters using environment variables

[ -z ${BOOTSTRAP_SERVER+x} ] && BOOTSTRAP_SERVER=""
[ -z ${COMMAND_CONFIG+x} ] && COMMAND_CONFIG=""
[ -z ${DEBUG+x} ] && DEBUG="n"

PLUGIN_PATH="$(dirname "${BASH_SOURCE[0]}")"
CONNECT_BIN=""

# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # e.g. return value is 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
  case "$1" in
  --bootstrap-server)
    BOOTSTRAP_SERVER="$2"
    shift 2
    ;;
  --command-config)
    COMMAND_CONFIG="$2"
    shift 2
    ;;
  -h | --help)
    echo "$HELP"
    exit 0
    ;;
  -d | --debug)
    DEBUG=y
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    echo "$1 $2 Programming error"
    exit 3
    ;;
  esac
done

##################################### Check Arguments

if [ -z "$BOOTSTRAP_SERVER" ]; then
  echo "--bootstrap-server is missing"
  echo "$HELP"
  exit 1
fi

if [ -n "$COMMAND_CONFIG" ] && [ ! -f "$COMMAND_CONFIG" ]; then
  echo "no such file $COMMAND_CONFIG"
  exit 1
fi

if [ ! -f "$PLUGIN_PATH/kafka-backup.jar" ]; then
  echo "Cannot find the kafka-backup.jar in $PLUGIN_PATH. Please set --backup-jar accordingly"
  exit 1
fi

# Running in distributed mode.
if [ -n "$(command -v connect-distributed.sh)" ]; then
  CONNECT_BIN="connect-distributed.sh"
fi

if [ -n "$(command -v connect-distributed)" ]; then
  CONNECT_BIN="connect-distributed"
fi

if [ -z "$CONNECT_BIN" ]; then
  echo "Cannot find connect-distributed or connect-distributed.sh in PATH. please add it"
  exit 1
fi

##################################### Create Worker Configs

# Distributed Worker Config

WORKER_CONFIG="$WORKING_DIR/distributed.properties"

# Replication factors are set to 1 because I'm using a 1 broker cluster, should remove it if there's at least 3.

cat <<EOF >"$WORKER_CONFIG"
bootstrap.servers=$BOOTSTRAP_SERVER
key.converter=org.apache.kafka.connect.converters.ByteArrayConverter
value.converter=org.apache.kafka.connect.converters.ByteArrayConverter
header.converter=org.apache.kafka.connect.converters.ByteArrayConverter
key.converter.schemas.enable=false
value.converter.schemas.enable=false
offset.flush.interval.ms=10000
task.shutdown.graceful.timeout.ms=300000
group.id=kafka-backup-cluster
config.storage.topic=kafka-backup-distributed-config
offset.storage.topic=kafka-backup-distributed-offset
status.storage.topic=kafka-backup-distributed-status
config.storage.replication.factor=1
offset.storage.replication.factor=1
status.storage.replication.factor=1
plugin.path=$PLUGIN_PATH
EOF

if [ -n "$COMMAND_CONFIG" ]; then
  cat "$COMMAND_CONFIG" >>"$WORKER_CONFIG"
fi

LOG4J_CONFIG="$WORKING_DIR/log4j.properties"

if [ "$DEBUG" == "y" ]; then
  echo "$WORKER_CONFIG:"
  sed 's/^/> /' "$WORKER_CONFIG"
  echo
  cat <<EOF >"$LOG4J_CONFIG"
log4j.rootLogger=INFO, stdout
log4j.logger.de.azapps.kafkabackup=DEBUG
log4j.logger.org.apache.zookeeper=ERROR
log4j.logger.org.I0Itec.zkclient=ERROR
log4j.logger.org.reflections=ERROR
log4j.logger.org.glassfish=ERROR

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=[%d] %p %m (%c)%n
EOF
else
  cat <<EOF >"$LOG4J_CONFIG"
log4j.rootLogger=WARN, stdout
log4j.logger.de.azapps.kafkabackup=INFO
log4j.logger.org.apache.zookeeper=ERROR
log4j.logger.org.I0Itec.zkclient=ERROR
log4j.logger.org.reflections=ERROR
log4j.logger.org.glassfish=ERROR

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=[%d] %p %m (%c)%n
EOF
fi

##################################### Log Configuration & Worker Start

KAFKA_LOG4J_OPTS="-Dlog4j.configuration=file:${LOG4J_CONFIG}"

"$CONNECT_BIN" "$WORKER_CONFIG"