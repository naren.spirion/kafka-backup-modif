#!/bin/bash
# saner programming env: these switches turn some bugs into errors
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test >/dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
  # shellcheck disable=SC2016
  echo '`getopt --test` failed in this environment.'
  exit 1
fi

WORKING_DIR="$(mktemp -d -t kafka-backup.XXXXXX)"

# Cleanup after SIGTERM/SIGINT
_term() {
  rm -r "$WORKING_DIR"
}

trap _term SIGTERM
trap _term INT

##################################### Parse arguments

OPTIONS="h"
LONGOPTS=bootstrap-server:,source-dir:,bucket-name:,topics:,max-source-tasks:,batch-size:,from-timestamp:,to-timestamp:,command-config:,help,debug

HELP=$(
  cat <<END
--bootstrap-server  [REQUIRED] The Kafka server to connect to
--source-dir        [REQUIRED] Directory where the backup files are found
--bucket-name       [REQUIRED] AWS S3 Bucket where the backup files are stored
--topics            [REQUIRED] List of topics to restore
--max-source-tasks             The maximum number of backup (restore) source tasks permitted. Default: 1.
                               [RECOMMENDED MAXIMUM: Equivalent to the number of topic partitions to be restored.]
--batch-size                   Batch size (Default: 1MiB)
--from-timestamp               Approximate point of time from which
                               records in the specified topics need to be restored.
                               Format: yyyy-mm-ddThh:mm:ssZ (UTC time)
                               *If this option is not provided,
                               the tool will RESUME FROM WHERE IT LEFT OFF
                               (or start from the beginning if it cannot find its previous position)
--to-timestamp                 Approximate point of time until which
                               records in the specified topics need to be restored.
                               Format: yyyy-mm-ddThh:mm:ssZ (UTC time)
                               *If this option is not provided,
                               the tool will restore records until
                               the latest record that was backed up
                               (approximately around the time of instantiation
                               of this source connector)
--command-config    <FILE>     Property file containing configs to be
                               passed to Admin Client. Only useful if you have additional connection options
--help                         Prints this message
--debug                        Print Debug information (if using the environment variable, set it to 'y')

You can also set all parameters using environment variables. Use CAPITAL LETTERS and underscores (_) instead of dashes (-).
E.g. BOOTSTRAP_SERVER instead of --bootstrap-server
END
)

[ -z ${BOOTSTRAP_SERVER+x} ] && BOOTSTRAP_SERVER=""
[ -z ${SOURCE_DIR+x} ] && SOURCE_DIR=""
[ -z ${BUCKET_NAME+x} ] && BUCKET_NAME=""
[ -z ${TOPICS+x} ] && TOPICS=""
[ -z ${BATCH_SIZE+x} ] && BATCH_SIZE="$((1 * 1024 * 1024))"
[ -z ${COMMAND_CONFIG+x} ] && COMMAND_CONFIG=""
[ -z ${DEBUG+x} ] && DEBUG="n"
[ -z ${FROM_TIMESTAMP+x} ] && FROM_TIMESTAMP=""
[ -z ${TO_TIMESTAMP+x} ] && TO_TIMESTAMP=""
[ -z ${MAX_SOURCE_TASKS+x} ] && MAX_SOURCE_TASKS=1

PLUGIN_PATH="$(dirname "${BASH_SOURCE[0]}")"
CONNECT_BIN=""

# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # e.g. return value is 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
  case "$1" in
  --bootstrap-server)
    BOOTSTRAP_SERVER="$2"
    shift 2
    ;;
  --source-dir)
    SOURCE_DIR="$2"
    shift 2
    ;;
  --bucket-name)
    BUCKET_NAME="$2"
    shift 2
    ;;
  --topics)
    TOPICS="$2"
    shift 2
    ;;
  --max-source-tasks)
    MAX_SOURCE_TASKS="$2"
    shift 2
    ;;
  --batch-size)
    BATCH_SIZE="$2"
    shift 2
    ;;
  --from-timestamp)
    FROM_TIMESTAMP="$2"
    shift 2
    ;;
  --to-timestamp)
    TO_TIMESTAMP="$2"
    shift 2
    ;;
  --command-config)
    COMMAND_CONFIG="$2"
    shift 2
    ;;
  -h | --help)
    echo "$HELP"
    exit 0
    ;;
  -d | --debug)
    DEBUG=y
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    echo "$1 $2 Programming error"
    exit 3
    ;;
  esac
done

##################################### Check arguments

if [ -z "$BOOTSTRAP_SERVER" ]; then
  echo "--bootstrap-server is missing"
  echo "$HELP"
  exit 1
fi

if [ -z "$SOURCE_DIR" ]; then
  echo "--source-dir is missing"
  echo "$HELP"
  exit 1
fi

if [ -z "$BUCKET_NAME" ]; then
  echo "--bucket-name is missing"
  echo "$HELP"
  exit 1
fi

if [ ! -d "$SOURCE_DIR" ]; then
  echo "Directory $SOURCE_DIR does not exist."
  exit 1
fi

if [ -z "$TOPICS" ]; then
  echo "--topics is missing"
  echo "$HELP"
  exit 1
fi

if [ -n "$COMMAND_CONFIG" ] && [ ! -f "$COMMAND_CONFIG" ]; then
  echo "no such file $COMMAND_CONFIG"
  exit 1
fi

if [ ! -f "$PLUGIN_PATH/kafka-backup.jar" ]; then
  echo "Cannot find the kafka-backup.jar in $PLUGIN_PATH. Please set --backup-jar accordingly"
  exit 1
fi

##################################### Connector Configuration

CONNECTOR_JSON='{
"name" : "backup-source",
"connector.class" : "de.azapps.kafkabackup.source.BackupSourceConnector",
"tasks.max" : "'"$MAX_SOURCE_TASKS"'",
"topics" : "'"$TOPICS"'",
"key.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"value.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"header.converter" : "org.apache.kafka.connect.converters.ByteArrayConverter",
"source.dir" : "'"$SOURCE_DIR"'",
"batch.size" : "'"$BATCH_SIZE"'",
"from.timestamp" : "'"$FROM_TIMESTAMP"'",
"to.timestamp" : "'"$TO_TIMESTAMP"'",
"bucket.name" : "'"$BUCKET_NAME"'",
"cluster.bootstrap.servers" : "'"$BOOTSTRAP_SERVER"'",
"cluster.key.deserializer" : "org.apache.kafka.common.serialization.ByteArrayDeserializer",
"cluster.value.deserializer" : "org.apache.kafka.common.serialization.ByteArrayDeserializer"'

JSON_LOCATION="$WORKING_DIR/configuration.json"

echo "$CONNECTOR_JSON" >"$JSON_LOCATION"

echo "}" >>"$JSON_LOCATION"

cat "$JSON_LOCATION"

# JSON file is sent to Kafka Connect via the REST API (contains the source connector configurations).
curl -i -X PUT -H "Content-Type:application/json" http://localhost:8083/connectors/backup-source/config -d @"$JSON_LOCATION"
echo "Distributed backup source connector posted."
# Delete the working directory.
_term