# Kafka Backup (modified for use with AWS S3)

Note: This is a modification of the [existing](https://github.com/itadventurer/kafka-backup/releases) Kafka Backup tool.

Kafka Backup is a tool to back up and restore your Kafka data
including all (configurable) topic data, as well as consumer
group offsets.

Currently `kafka-backup` supports backup and restore to / from the file
system, I have extended the tool to support backups to Amazon S3, although proper testing is pending.

It is designed as two connectors for Kafka Connect: A sink connector (backing data up) and a source connector
(restoring data).

The connectors were initially intended to be run in standalone mode, due to the fact that backups went to the filesystem,
and not some common storage endpoint.

I have modified the sink (backup) connector and source (restore) connector to run in distributed mode.
The sink connector can support multiple tasks when performing backups to S3.
The source connector presently supports one task.

## Features

* Backup & restore topic data.
* Backup & restore consumer - group offsets.
* Supports backup / restore to / from the local filesystem, as well as S3 buckets* (might need more testing).
* Released as a JAR file.

# Getting Started

**Build from source**

Just run `./gradlew shadowJar` in the root directory of Kafka Backup. The required CLI tools will be in the `bin` directory.

## Start Kafka Backup

```sh
# Starting a Kafka Connect Worker Process...
./start_backup_cluster_worker.sh --bootstrap-server localhost:9092 --debug
```

Make sure that your AWS credentials are set as [environment variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html#envvars-set).
(AWS CLI installation is not required). Ensure that you have sufficient permissions on the bucket of interest.
If backing up to a new S3 bucket, create it manually beforehand.

The above script starts a single distributed Kafka Connect Worker, with `group.id` = `kafka-backup-cluster`.
Ensure that your Kafka Cluster is up and running beforehand, and change the hostname and port appropriately.

Meanwhile, open up another shell on the same machine and run the below script:

```sh
# Setting up a Kafka Backup Sink Connector (overrides existing configuration if rerun)...
./backup_distributed.sh --bootstrap-server localhost:9092 \
    --target-dir /path/to/backup/dir --topics 'topic1,topic2' --bucket-name samplebucket
```

Internally, this script is simply posting the sink connector configuration (provided through the arguments) and posting it to the Connect Worker.
The backup process then starts.

If you want the backup sink connector to automatically detect newly created topics, send `--topics.regex .+`. Every topic except `__consumer_offsets` gets backed up.

Passing `--bucketname null` is equivalent to simply backing up to the filesystem as usual.
Otherwise, the backup directory serves as a point where files are created and written into, before they are uploaded to the S3 bucket.

You can pass options via CLI arguments or using environment variables:

| Parameter                                   | Type/required? | Description                                                                                                          |
|---------------------------------------------|----------------|----------------------------------------------------------------------------------------------------------------------|
| `--bootstrap-server`<br/>`BOOTSTRAP_SERVER` | [REQUIRED]     | The Kafka server to connect to                                                                                       |
| `--target-dir`<br/>`TARGET_DIR`             | [REQUIRED]     | Directory where the backup files should be stored                                                                    |
| `--bucket-name`<br/>`BUCKET_NAME`           | [REQUIRED]     | AWS S3 bucket where the backup files should be stored (use `--bucket-name null` if not backing up to S3)             |
| `--topics`<br/>`TOPICS`                     | <T1,T2,…>      | List of topics to be backed up. You must provide either `--topics` or `--topics-regex`. Not both                     |
| `--topics-regex`<br/>`TOPICS_REGEX`         |                | Regex of topics to be backed up. You must provide either `--topics` or `--topics-regex`. Not both                    |
| `--max-sink-tasks`<br/>`MAX_SINK_TASKS`     |                | The maximum number of backup sink tasks permitted. Default: 1.                                                       |
|                                             |                | RECOMMENDED MAXIMUM: Equivalent to the number of topic partitions to be backed up (in case of S3 backups).           |
|                                             |                | IMPORTANT: Do not make this parameter > 1 if using --bucket-name null (i.e. backing up to the file system).          |
|                                             |                | Your Kafka Connect cluster should also contain only a single distributed worker in that case.                        |
| `--max-segment-size`<br/>`MAX_SEGMENT_SIZE` |                | Size of the backup segments in bytes DEFAULT: 1GiB                                                                   |
| `--command-config`<br/>`COMMAND_CONFIG`     | <FILE>         | Property file containing configs to be passed to Admin Client. Only useful if you have additional connection options |
| `--debug`<br/>`DEBUG=y`                     |                | Print Debug information                                                                                              |
| `--help`                                    |                | Prints this message                                                                                                  |

**Kafka Backup does not stop!** The Backup process is a continuous background job that runs forever as Kafka models data as a stream without end. See [Issue 52: Support point-in-time snapshots](https://github.com/itadventurer/kafka-backup/issues/52) for more information.

## Restore data

```sh
restore_distributed.sh --bootstrap-server localhost:9092 \
    --target-dir /path/to/dir/to/restore/from --topics 'topic1,topic2' --bucket-name samplebucket
```

Internally, this script is simply posting the source connector configuration (provided through the arguments) and posting it to the Connect Worker.
Topics to restore must be specified through a list. At present, the source connector doesn't automatically identify topics in the backup directory / bucket.

You can pass options via CLI arguments or using environment variables:


| Parameter                                   | Type/required? | Description                                                                                                                                                                                                                |
|---------------------------------------------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `--bootstrap-server`<br/>`BOOTSTRAP_SERVER` | [REQUIRED]     | The Kafka server to connect to                                                                                                                                                                                             |
| `--source-dir`<br/>`SOURCE_DIR`             | [REQUIRED]     | Directory where the backup files are found                                                                                                                                                                                 |
| `--bucket-name`<br/>`BUCKET_NAME`           | [REQUIRED]     | AWS S3 bucket where the backup files are found (use `--bucket-name null` if not backing up from S3)                                                                                                                        |
| `--topics`<br/>`TOPICS`                     | [REQUIRED]     | List of topics to restore                                                                                                                                                                                                  |
| `--max-source-tasks`<br/>`MAX_SOURCE_TASKS` |                | The maximum number of backup (restore) source tasks permitted. Default: 1.                                                                                                                                                 |
|                                             |                | RECOMMENDED MAXIMUM: Equivalent to the number of topic partitions to be restored.                                                                                                                                          |
|                                             |                | Providing this option is currently redundant as only one source task is supported at the moment.                                                                                                                           |
| `--batch-size`<br/>`BATCH_SIZE`             |                | Batch size (Default: 1MiB)                                                                                                                                                                                                 |
| `--from-timestamp`<br/>`FROM_TIMESTAMP`     |                | Approximate point of time from which records in the specified topics need to be restored.                                                                                                                                  |
|                                             |                | Format: yyyy-mm-ddThh:mm:ssZ (UTC time)                                                                                                                                                                                    |
|                                             |                | *If this option is not provided, the tool will RESUME FROM WHERE IT LEFT OFF (or start from the beginning if it cannot find its previous position)                                                                         |
| `--to-timestamp`<br/>`TO_TIMESTAMP`         |                | Approximate point of time until which records in the specified topics need to be restored.                                                                                                                                 |
|                                             |                | Format: yyyy-mm-ddThh:mm:ssZ (UTC time)                                                                                                                                                                                    |
|                                             |                | *If this option is not provided, the tool will restore records until the latest record that was backed up                                                                                                                  |
|                                             |                | (approximately around the time of instantiation of this source connector)                                                                                                                                                  |
| `--command-config`<br/>`COMMAND_CONFIG`     | <FILE>         | Property file containing configs to be passed to Admin Client. Only useful if you have additional connection options                                                                                                       |
| `--help`<br/>`HELP`                         |                | Prints this message                                                                                                                                                                                                        |
| `--debug`<br/>`DEBUG`                       |                | Print Debug information (if using the environment variable, set it to 'y')                                                                                                                                                 |

## More Documentation (From the original contributors)

* [FAQ](./docs/FAQ.md)
* [High Level
  Introduction](./docs/Blogposts/2019-06_Introducing_Kafka_Backup.md)
* [Comparing Kafka Backup
  Solutions](./docs/Comparing_Kafka_Backup_Solutions.md)
* [Architecture](./docs/Kafka_Backup_Architecture.md)
* [Tooling](./docs/Tooling.md)

## License

This project is licensed under the Apache License Version 2.0 (see
[LICENSE](./LICENSE)).
